# Base image
FROM ubuntu:24.04

# Set metadata labels
LABEL maintainer="Dr. Denis Jacob Machado <dmachado@charlotte.edu>"
LABEL description="This Docker image installs Python 3.11 with dependencies for bioinformatics, including BioPython, Parsl, BLAST+, HMMER, and MAFFT. It also includes a custom script FLAVi2.py with the necessary input files and directories."

# Set environment variables
ENV DEBIAN_FRONTEND=noninteractive
ENV PATH="/usr/local/bin:$PATH"

# Update and install essential tools, Python, and bioinformatics software
RUN apt-get -y update
RUN apt-get install -y software-properties-common
RUN add-apt-repository ppa:deadsnakes/ppa -y
RUN apt-get -y update

# Install BLAST and HMMER
RUN apt install ncbi-blast+ -y
RUN apt install hmmer -y
RUN hmmbuild -h

# Install python3 and pip3
RUN apt-get install -y python3
RUN apt-get install -y python3-pip

# Install the latest versions of Python dependencies with pip
RUN pip3 install --break-system-packages biopython pandas parsl

# Set the working directory to root
WORKDIR /

# Copy files and directories to root
COPY FLAVi2.py .
COPY example.fasta .
COPY peptides ./peptides
COPY hmms ./hmms

# Preprocess HMMER database with hmmpress
RUN cd hmms && hmmpress -f FLAVi.hmm > /dev/null 2>&1

# Clean up
RUN apt-get clean && rm -rf /var/lib/apt/lists/*

# Set the default command to run the main Python script with the specified arguments
CMD ["python3", "FLAVi2.py", "-i", "example.fasta", "-p", "peptides", "-m", "hmms"]
