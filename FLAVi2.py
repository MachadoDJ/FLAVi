#!/usr/bin/env python3
# -*- coding: utf-8 -*-

####################
# IMPORT LIBRARIES #
####################

import argparse
import glob
import os
import re
import sys
import shutil
from io import StringIO
try:  # Import BioPython
    from Bio import SeqIO
    from Bio.Seq import Seq
except:
    sys.stderr.write("!ERROR: Failed to load BioPython.\n")
    exit()
try:  # Import Pandas
    import pandas as pd
except:
    sys.stderr.write("!ERROR: Failed to load Pandas.\n")
    exit()
try:  # Import Parsl
    import parsl
    from parsl import python_app, bash_app, ThreadPoolExecutor
except:
    sys.stderr.write("!ERROR: Failed to load Parsl.\n")
    exit()

#################
# ENVIRONMENTAL #
#################

stop_codons = ["TAA", "TAG", "TGA"]

################
# PARSL CONFIG #
################

def create_parsl_config(max_threads):
    """
    Create a Parsl configuration for task execution.
    Parameters:
        max_threads (int): The maximum number of threads to use for parallel processing.
    Returns:
        dict: Configuration dictionary for Parsl.
    """
    return {
        "executors": [
            ThreadPoolExecutor(max_threads=max_threads, label='parallel'),
            ThreadPoolExecutor(max_threads=1, label='serial'),
        ],
        "strategy": None
    }

###################
# PARSL FUNCTIONS #
###################

@bash_app(executors=['parallel'])
def makeblastdb(input_file, type, molecule_type):
    """
    Prepare a BLAST database for searching.
    Parameters:
        input_file (str): The input file to convert into a BLAST database.
        type (str): The input file type.
        molecule_type (str): Type of molecule, e.g., 'prot' for protein databases.
    Returns:
        str: Command string to be executed.
    """
    return f"makeblastdb -in '{input_file}' -input_type {type} -dbtype {molecule_type} -parse_seqids > /dev/null 2>&1"

@bash_app(executors=['parallel'])
def blastp(database_name, input_file, output_file, num_threads=1, evalue=1e-03, format=6, num_sequences=1):
    """
    Run a BLASTP search on a database.
    Parameters:
        database_name (str): BLAST database name.
        input_file (str): Query file.
        output_file (str): Output file to save BLASTP results.
        num_threads (int): Number of threads to use for BLASTP.
        evalue (float): E-value threshold for BLASTP.
        format (int): Output format of BLASTP.
        num_sequences (int): Maximum number of sequences to return.
    Returns:
        str: Command string to execute BLASTP.
    """
    return f"blastp -db '{database_name}' -query '{input_file}' -out '{output_file}' -evalue {evalue} -outfmt {format} -max_target_seqs {num_sequences} -num_threads {num_threads} > /dev/null 2>&1"

@bash_app(executors=['serial'])
def hmmpress(hmms_dir):
    """
    Format HMM profile for HMMER.
    Parameters:
        hmms_dir (str): Directory containing the HMM profile.
    Returns:
        str: Command string to format HMM profile.
    """
    return f"hmmpress -f '{hmms_dir}/FLAVi.hmm' > /dev/null 2>&1"

@bash_app(executors=['serial'])
def hmmscan(hmms_dir, input_file, output_prefix, num_threads, evalue=10):
    """
    Run HMMER scan on a sequence file.
    Parameters:
        hmms_dir (str): Directory containing HMM profile.
        input_file (str): Query file.
        output_prefix (str): Output file prefix.
        num_threads (int): Number of threads for HMMER.
        evalue (float): E-value threshold for HMMER scan.
    Returns:
        str: Command string for HMMER scan.
    """
    return f"hmmscan --noali --notextw --cpu {num_threads} -E {evalue} --domtblout '{output_prefix}_hmmscan.txt' '{hmms_dir}/FLAVi.hmm' '{input_file}' > /dev/null 2>&1"

@bash_app(executors=['serial'])
def mafft(filename, num_threads):
    """
    Align sequences using MAFFT.
    Parameters:
        filename (str): File containing sequences to align.
        num_threads (int): Number of threads for MAFFT.
    Returns:
        str: Command string for MAFFT alignment.
    """
    return f"mafft --auto --thread {num_threads} '{filename}' > '{filename}.align' 2> /dev/null"

###############
# FLAVI CLASS #
###############

class Flavi:
    """
    Class for managing viral genome analysis including BLAST, HMMER, and alignment processes.
    """

    def __init__(self, input_file, peptides_dir, hmms_dir, output_prefix, threads):
        """
        Initialize Flavi class with input parameters.
        Parameters:
            input_file (str): Path to input genome file in FASTA format.
            peptides_dir (str): Directory with peptide files.
            hmms_dir (str): Directory with HMM files.
            output_prefix (str): Prefix for output files.
            threads (int): Number of threads to use.
        """
        self.input_file = input_file
        self.peptides_dir = peptides_dir
        self.hmms_dir = hmms_dir
        self.output_prefix = output_prefix
        self.threads = threads
        self.genomes = {}  # Stores genome sequences
        self.annotations = {}  # Stores annotations for each genome

    def read(self):
        """
        Reads genome sequences from the input file and stores them in self.genomes.
        Input:
            None
        Output:
            Updates self.genomes dictionary with sequence information.
        """
        with open(self.input_file, "r") as file:
            for record in SeqIO.parse(file, "fasta"):
                header = str(record.id)
                sequence = str(record.seq).strip().upper()
                self.genomes[header] = sequence

    def find_lorfs(self):
        """
        Identify the longest ORF for each genome sequence.
        Input:
            None
        Output:
            Updates self.annotations with ORF details.
        """
        for header in self.genomes:
            sequence = self.genomes[header]
            lorf_start, lorf_end, lorf_frame, lorf_sequence = find_lorf(sequence)
            if lorf_frame < 0:
                self.genomes[header] = reverse_complement(self.genomes[header])
            if not header in self.annotations:
                self.annotations[header] = {}
            self.annotations[header]["polyprotein"] = {
                "nucl_start": lorf_start,
                "nucl_end": lorf_end,
                "frame": lorf_frame,
                "sequence": str(Seq(lorf_sequence).translate(table=1))
            }

    def print_lorfs(self):
        """
        Write identified ORFs to individual FASTA files.
        Input:
            None
        Output:
            Creates .faa files containing polyprotein ORF sequences.
        """
        for header in self.annotations:
            handle = open(f"{self.output_prefix}_{header}.faa", "w")
            polyprotein = self.annotations[header]["polyprotein"]["sequence"]
            frame = self.annotations[header]["polyprotein"]["frame"]
            nucl_start = self.annotations[header]["polyprotein"]["nucl_start"]
            nucl_end = self.annotations[header]["polyprotein"]["nucl_end"]
            if frame < 0:
                handle.write(f">{header} polyprotein position={nucl_end}-{nucl_start} frame={frame}\n{polyprotein}\n")
            else:
                handle.write(f">{header} polyprotein position={nucl_start}-{nucl_end} frame={frame}\n{polyprotein}\n")
            handle.close()

    def blast(self):
        """
        Execute BLASTP searches for each peptide file against the generated BLAST databases.
        Input:
            None
        Output:
            Updates self.annotations with BLAST results, including start, end positions, 
            e-value, and score for each peptide sequence.
        """
        # Create BLAST databases for polyprotein sequences
        processes = [makeblastdb(f"{self.output_prefix}_{header}.faa", "fasta", "prot") 
                     for header in self.annotations 
                     if not file_exists(f"{self.output_prefix}_{header}.faa.pdb")]
        results = [result.result() for result in processes]  # Run each makeblastdb process

        parameters = []  # To store parameters for each BLASTP search

        # Set the maximum number of threads for BLASTP, here only one is used
        max_threads_for_blastp = 1

        # Iterate over peptide files and set up BLASTP search parameters
        for pep_file in glob.glob(f"{self.peptides_dir}/*.pep"):
            pep_name = re.compile(r"([^\/]+)\.pep$").findall(pep_file)[0]  # Extract name from file path
            for header in self.annotations:
                parameters.append([f"{self.output_prefix}_{header}.faa", f"{pep_file}", 
                                   f"{self.output_prefix}_{header}_{pep_name}.tsv", max_threads_for_blastp])

        # Execute BLASTP search with parameters collected
        processes = [blastp(par[0], par[1], par[2]) for par in parameters]
        results = [result.result() for result in processes]  # Retrieve all results

        delete_empty_files(self.output_prefix)  # Remove any empty output files

        # Read and process BLASTP output, updating annotations
        for target in self.annotations:
            for tsv_name in glob.glob(f"{self.output_prefix}_{target}_*.tsv"):
                pep_name = re.compile(r"\_([^\_]+)\.tsv$").findall(tsv_name)[0]

                # Read BLAST output into a DataFrame
                df = pd.read_csv(tsv_name, sep="\t", header=None)

                # Define column headers based on BLAST output format 6
                headers = ["qseqid", "sseqid", "pident", "length", "mismatch", "gapopen", 
                           "qstart", "qend", "sstart", "send", "evalue", "bitscore"]
                df.columns = headers

                # Sort BLAST results based on identity, length, bitscore, and evalue
                df = df.sort_values(by=["pident", "length", "bitscore", "evalue"], 
                                    ascending=[False, False, False, True])
                first_row = df.iloc[0]  # Retrieve the best hit

                # Extract relevant values from the first row
                sstart_value = first_row["sstart"]
                send_value = first_row["send"]
                evalue = first_row["evalue"]
                bitscore = first_row["bitscore"]

                # Update annotations with BLAST results
                if pep_name not in self.annotations[target]:
                    self.annotations[target][pep_name] = {}
                self.annotations[target][pep_name]["blastp"] = {
                    "start": sstart_value,
                    "end": send_value,
                    "evalue": evalue,
                    "score": bitscore
                }
        return  # Function returns nothing; results stored in self.annotations

    def hmmer(self):
        """
        Execute HMMER search to identify matching HMM profiles for the polyprotein sequences.
        Input:
            None
        Output:
            Updates self.annotations with HMMER results, including positions, e-value, and score for each profile.
        """
        build = False  # Tracks whether to build HMM database

        # Check if HMM database files are missing
        for db_file in ["FLAVi.hmm.h3f", "FLAVi.hmm.h3i", "FLAVi.hmm.h3m", "FLAVi.hmm.h3p"]:
            if not file_exists(f"{self.hmms_dir}/{db_file}"):
                sys.stderr.write(f"!WARNING: Cannot find file '{db_file}' in '{self.hmms_dir}'.\n")
                build = True

        # Build HMM database if necessary
        if build:
            sys.stderr.write(f"!Pressing profile HMM '{db_file}' in '{self.hmms_dir}'.\n")
            future = hmmpress(self.hmms_dir)
            result = future.result()

        # Run hmmscan on each sequence
        futures = [hmmscan(self.hmms_dir, f"{self.output_prefix}_{header}.faa", 
                           f"{self.output_prefix}_{header}", self.threads) 
                   for header in self.annotations]
        results = [result.result() for result in futures]  # Retrieve hmmscan results

        # Process hmmscan output files and update annotations
        for header in self.annotations:
            input_file = f"{self.output_prefix}_{header}_hmmscan.txt"
            if not file_exists(input_file):
                sys.stderr.write(f"!ERROR: HMMER output file {input_file} is missing.\n")
                exit()

            # Parse hmmscan output
            handle = open(input_file, "r")
            hits = []  # Collect all HMMER hits
            for line in handle.readlines():
                line = re.sub(r"#.*", "", line).strip()  # Remove comments and whitespace
                if line:
                    cells = re.compile(r"([^\s+]+)").findall(line)
                    # Extract values from line
                    sstart_value = int(cells[17])
                    send_value = int(cells[18])
                    pep_name = re.sub(r"_.*", "", str(cells[0]))
                    target = str(cells[3])
                    evalue = float(cells[12])
                    score = float(cells[13])
                    bias = float(cells[14])
                    hits.append([target, pep_name, sstart_value, send_value, evalue, score, bias])
            handle.close()

            # Sort hits by score, e-value, and bias
            hits = sorted(hits, key=lambda x: x[6])  # Sort by ascending bias
            hits = sorted(hits, key=lambda x: x[5], reverse=True)  # Sort by descending score
            hits = sorted(hits, key=lambda x: x[4])  # Sort by ascending e-value

            # Process each hit, avoiding duplicates
            avoid_repetition = []
            for target, pep_name, sstart_value, send_value, evalue, score, bias in hits:
                if pep_name in avoid_repetition:
                    continue
                if target not in self.annotations:
                    self.annotations[target] = {}
                if pep_name not in self.annotations[target]:
                    self.annotations[target][pep_name] = {}
                # Store HMMER results in annotations
                self.annotations[target][pep_name]["hmmer"] = {
                    "start": sstart_value,
                    "end": send_value,
                    "evalue": evalue,
                    "score": score
                }
                avoid_repetition.append(pep_name)  # Prevent repeated processing of the same peptide name
        return

    def output_clean(self):
        """
        Remove all temporary or output files generated during processing.
        Input:
            None
        Output:
            Cleans up any files in the current directory with a name matching self.output_prefix.
        """
        # Find all files matching the output prefix and delete them
        for output_file in glob.glob(f"{self.output_prefix}*"):
            os.remove(output_file)

    def calculate_predictions(self):
        """
        Generate predictions based on BLAST and HMMER results and write them to a CSV file.
        Input:
            None
        Output:
            Writes a CSV file containing the start and end positions, scores, and e-values for each prediction.
        """
        output = "Target,Gene,Nucl_Start,Nucl_End,Prot_Start,Prot_End,Blast_Start,Blast_End,Blast_Evalue,Blast_Score,HMMER_Start,HMMER_End,HMMER_Evalue,HMMER_Score\n"

        # Iterate through each target (genome) to calculate predictions
        for target in self.annotations:
            # Retrieve polyprotein data
            pol_start = self.annotations[target]["polyprotein"]["nucl_start"]
            pol_end = self.annotations[target]["polyprotein"]["nucl_end"]
            pol_frame = self.annotations[target]["polyprotein"]["frame"]
            pol_prot_start = 0
            pol_prot_end = len(self.annotations[target]["polyprotein"]["sequence"]) - 1

            # Check if polyprotein is on the reverse strand
            if pol_frame < 0:
                output += f"{target} (reverse complement),polyprotein,{pol_end},{pol_start},{pol_prot_end},{pol_prot_start},NA,NA,NA,NA,NA,NA,NA,NA\n"
            else:
                output += f"{target},polyprotein,{pol_start},{pol_end},{pol_prot_start},{pol_prot_end},NA,NA,NA,NA,NA,NA,NA,NA\n"

            # Process each peptide prediction
            for pep_name in self.annotations[target]:
                if pep_name == "polyprotein":
                    continue

                # Extract BLAST results if available
                if "blastp" in self.annotations[target][pep_name]:
                    blastp_start = self.annotations[target][pep_name]["blastp"]["start"]
                    blastp_end = self.annotations[target][pep_name]["blastp"]["end"]
                    blastp_evalue = self.annotations[target][pep_name]["blastp"]["evalue"]
                    blastp_score = self.annotations[target][pep_name]["blastp"]["score"]
                else:
                    blastp_start = "NA"
                    blastp_end = "NA"
                    blastp_evalue = "NA"
                    blastp_score = "NA"

                # Extract HMMER results if available
                if "hmmer" in self.annotations[target][pep_name]:
                    hmmer_start = self.annotations[target][pep_name]["hmmer"]["start"]
                    hmmer_end = self.annotations[target][pep_name]["hmmer"]["end"]
                    hmmer_evalue = self.annotations[target][pep_name]["hmmer"]["evalue"]
                    hmmer_score = self.annotations[target][pep_name]["hmmer"]["score"]
                else:
                    hmmer_start = "NA"
                    hmmer_end = "NA"
                    hmmer_evalue = "NA"
                    hmmer_score = "NA"

                # Determine start and end of the protein region from BLAST and HMMER results
                if blastp_start == "NA" and hmmer_start == "NA":
                    prot_start = "NA"
                    prot_end = "NA"
                elif blastp_start == "NA":
                    prot_start = hmmer_start
                    prot_end = hmmer_end
                elif hmmer_start == "NA":
                    prot_start = blastp_start
                    prot_end = blastp_end
                else:
                    _, prot_start, prot_end, _ = sorted([blastp_start, blastp_end, hmmer_start, hmmer_end])

                # Calculate nucleotide positions for protein region if start and end are valid
                if prot_start == "NA":
                    nucl_start = "NA"
                    nucl_end = "NA"
                else:
                    nucl_start = 3 * (prot_start) + pol_start
                    nucl_end = 3 * (prot_end) + pol_start + 2
                    # Verify nucleotide range is divisible by 3 (for codon alignment)
                    if (nucl_end - nucl_start + 1) % 3 != 0:
                        sys.stderr.write("!ERROR: {target}-{pep_name}: {nucl_start}-{nucl_end}: Not divisible by 3.\n")
                        exit()

                # Add results for this peptide to the output string
                output += f"{target},{pep_name},{nucl_start},{nucl_end},{prot_start},{prot_end},{blastp_start},{blastp_end},{blastp_evalue},{blastp_score},{hmmer_start},{hmmer_end},{hmmer_evalue},{hmmer_score}\n"

                # Update annotation dictionary with calculated nucleotide and protein positions
                self.annotations[target][pep_name]["nucl_start"] = nucl_start
                self.annotations[target][pep_name]["nucl_end"] = nucl_end
                self.annotations[target][pep_name]["prot_start"] = prot_start
                self.annotations[target][pep_name]["prot_end"] = prot_end

        # Convert the output to a DataFrame and sort by nucleotide start
        df = pd.read_csv(StringIO(output))
        df_sorted = df.sort_values(by=['Nucl_Start', 'Target'])

        # Save sorted DataFrame to a CSV file
        df_sorted.to_csv(f"{self.output_prefix}_predictions.csv", index=False)
        return

    def export_fasta(self):
        """
        Export annotated nucleotide and protein sequences in FASTA format.
        Input:
            None
        Output:
            Creates .fna files with nucleotide sequences and .faa files with protein sequences for each target.
        """
        # Iterate through each target in the annotations
        for target in self.annotations:
            # Iterate through each peptide or polyprotein entry
            for pep_name in self.annotations[target]:
                nucl_start = self.annotations[target][pep_name]["nucl_start"]
                nucl_end = self.annotations[target][pep_name]["nucl_end"]

                # Extract nucleotide sequence for current peptide
                nucl_seq = self.genomes[target][nucl_start : nucl_end + 1]

                # Check sequence length is divisible by 3 for correct translation
                if len(nucl_seq) % 3 != 0:
                    sys.stderr.write(f"!WARNING: {target}-{pep_name}: Nucleotide sequence length {len(nucl_seq)} is not divisible by 3. Will be completed witn Ns at the 3' end.\n")
                    nucl_seq += "N" * (3 - (len(nucl_seq) % 3))

                # Write nucleotide sequence to .fna file
                with open(f"{self.output_prefix}_{pep_name}.fna", "a") as handle:
                    handle.write(f">{target} {pep_name} {nucl_start}-{nucl_end}\n{nucl_seq}\n")

                # Process protein sequences for polyprotein and peptide
                if pep_name == "polyprotein":
                    prot_seq = self.annotations[target]["polyprotein"]["sequence"]
                    prot_start = 0
                    prot_end = len(prot_seq)
                else:
                    prot_start = self.annotations[target][pep_name]["prot_start"]
                    prot_end = self.annotations[target][pep_name]["prot_end"]
                    prot_seq = self.annotations[target]["polyprotein"]["sequence"][prot_start : prot_end + 1]

                # Write protein sequence to .faa file
                with open(f"{self.output_prefix}_{pep_name}.faa", "a") as handle:
                    handle.write(f">{target} {pep_name} {prot_start}-{prot_end}\n{prot_seq}\n")
        return

###############
# ALIGN CLASS #
###############

class Align:
    """
    Class for handling sequence alignments, converting protein alignments to nucleotide alignments, 
    and exporting in NEXUS format.
    """

    def __init__(self, output_prefix, threads):
        """
        Initialize the Align class with output prefix and thread count.
        Parameters:
            output_prefix (str): Prefix for output files.
            threads (int): Number of threads to use for parallel processing.
        """
        self.prefix = output_prefix
        self.threads = threads
        self.nucleotides = {}  # Dictionary to store nucleotide sequences
        self.proteins = {}  # Dictionary to store protein sequences
        self.matrix = {}  # Dictionary for aligned matrix of sequences
        self.partitions = {}  # Dictionary for partition data for each gene

    def read_fna(self):
        """
        Read all nucleotide sequences from FASTA files with a .fna extension.
        Input:
            None
        Output:
            Populates self.nucleotides with nucleotide sequences for each gene.
        """
        # Iterate over each .fna file with the specified prefix
        for filename in glob.glob(f"{self.prefix}*fna"):
            gene = re.compile(rf"{self.prefix}_(.+?)\.fna").findall(filename)[0].lower()  # Extract gene name from filename
            self.nucleotides[gene] = {}  # Initialize dictionary for each gene

            # Read each sequence in the .fna file
            with open(filename, "r") as input:
                for record in SeqIO.parse(input, "fasta"):
                    self.nucleotides[gene][record.id] = str(record.seq)  # Store sequence in nucleotides dictionary

    def align_faa(self):
        """
        Perform multiple sequence alignment on protein sequences using MAFFT.
        Input:
            None
        Output:
            Aligns protein sequences and creates .align files for each .faa file.
        """
        # Run MAFFT alignment on each .faa file in parallel
        processes = [mafft(filename, self.threads) for filename in glob.glob(f"{self.prefix}*faa")]
        results = [result.result() for result in processes]  # Retrieve alignment results for each file

    def read_faa_alignments(self):
        """
        Read aligned protein sequences from files with .faa.align extension.
        Input:
            None
        Output:
            Populates self.proteins dictionary with aligned protein sequences for each gene.
        """
        # Iterate over each .faa.align file with the specified prefix
        for filename in glob.glob(f"{self.prefix}*faa.align"):
            gene = re.compile(rf"{self.prefix}_(.+?)\.faa.align").findall(filename)[0].lower()  # Extract gene name
            self.proteins[gene] = {}  # Initialize dictionary for each gene

            # Read each sequence in the .faa.align file
            with open(filename, "r") as input:
                for record in SeqIO.parse(input, "fasta"):
                    self.proteins[gene][record.id] = str(record.seq)  # Store aligned sequence in proteins dictionary

    def prot2nucl(self):
        """
        Convert aligned protein sequences back to nucleotide alignments.
        Input:
            None
        Output:
            Creates .fna.align files containing nucleotide alignments corresponding to protein alignments.
        """
        # Iterate over each gene's aligned protein sequences
        for gene in self.proteins:
            gene = gene.lower()
            output_name = f"{self.prefix}_{gene}.fna.align"  # Output file for nucleotide alignment
            with open(output_name, "w") as handle:
                # Iterate through each sequence in the protein alignment
                for header in self.proteins[gene]:
                    aligned_prot = self.proteins[gene][header]  # Aligned protein sequence
                    if aligned_prot[-1] == "*":
                        aligned_prot = aligned_prot[:-1]
                    unaliged_nucl = self.nucleotides[gene][header]  # Original nucleotide sequence
                    if unaliged_nucl[-3:] in stop_codons:
                        unaliged_nucl = unaliged_nucl[:-3]
                    aligned_nucl = ""  # Initialize aligned nucleotide sequence
                    nucl_position = 0  # Start position in the unaligned nucleotide sequence

                    # Convert each amino acid to corresponding nucleotide codons
                    for position in range(len(aligned_prot)):
                        aa = aligned_prot[position]
                        if aa == "-":  # For gaps in protein alignment, add gaps to nucleotide sequence
                            aligned_nucl += "---"
                        else:
                            # Add corresponding codon from nucleotide sequence
                            aligned_nucl += unaliged_nucl[nucl_position: nucl_position + 3]
                            nucl_position += 3  # Move to next codon

                    # Check that alignment lengths for nucleotide and protein are consistent
                    if len(aligned_nucl) != 3 * len(aligned_prot):
                        sys.stderr.write(f"!ERROR processing file {self.prefix}_{gene}.fna.align: Nucleotide alignment length is not three times the length of the protein alignment length:\n>Unaligned nucleotide sequence\n{unaliged_nucl}\n>Aligned nucleotide sequence\n{aligned_nucl}\n>Aligned amino acids\n{aligned_prot}")
                        exit()

                    # Check that unaligned nucleotide sequence matches aligned version without gaps
                    check1 = remove_trailing_Ns(unaliged_nucl)
                    check2 = remove_trailing_Ns(aligned_nucl)
                    if not check2 in check1:
                        sys.stderr.write(f"!ERROR processing file {self.prefix}_{gene}.fna.align: The aligned nucleotide sequence is not equal or inside its unaligned version:\n>Unaligned nucleotide sequence\n{unaliged_nucl}\n>Aligned nucleotide sequence\n{aligned_nucl}\n")
                        exit()

                    # Write aligned nucleotide sequence to file
                    handle.write(f">{header}\n{aligned_nucl}\n")
                    # Update nucleotides dictionary with aligned sequence
                    self.nucleotides[gene][header] = aligned_nucl

    def fasta2nexus(self):
        """
        Convert nucleotide alignments into a NEXUS format matrix and create partitions file.
        Input:
            None
        Output:
            Creates a .nexus matrix file and a partitions file for sequence alignments.
        """
        all_genes = []  # List to store gene names
        all_headers = []  # List to store sequence headers
        align_len = {}  # Dictionary for alignment lengths by gene
        total_length = 0  # Cumulative length of all gene alignments

        # Collect gene names, sequence headers, and alignment lengths
        for gene in self.nucleotides:
            all_genes.append(gene)
            for header in self.nucleotides[gene]:
                all_headers.append(header)
                if gene not in align_len:
                    this_size = len(self.nucleotides[gene][header])
                    align_len[gene] = this_size  # Store alignment length for gene
                    total_length += this_size  # Update total alignment length

        # Sort genes and headers for consistent ordering
        all_genes = sorted(all_genes)
        all_headers = sorted(list(set(all_headers)))
        number_of_terminals = len(all_headers)

        # Add missing sequences as gaps if header is not present for any gene
        for gene in self.nucleotides:
            for header in all_headers:
                if header not in self.nucleotides[gene]:
                    self.nucleotides[gene][header] = "?" * align_len[gene]  # Fill with gaps

        # Prepare NEXUS format output
        nexus_output = f"#NEXUS\nBEGIN DATA;\n    DIMENSIONS NTAX={number_of_terminals} NCHAR={total_length};\n    FORMAT DATATYPE=DNA MISSING=? GAP=-;\n    MATRIX\n"

        # Add each sequence in the alignment to the NEXUS matrix
        for header in all_headers:
            nexus_output += f"        {header}    "
            for gene in all_genes:
                nexus_output += self.nucleotides[gene][header]  # Append nucleotide sequence
            nexus_output += "\n"
        nexus_output += "    ;\nEND;"

        # Prepare partitions for each gene in the alignment
        partitions_output = ""
        paritions_nexus_output = "#NEXUS\nBEGIN SETS;\n"
        last_position = 0  # Tracks cumulative position in the alignment

        # Add partition definitions for each gene
        for gene_no in range(0, len(all_genes)):
            gene = all_genes[gene_no]
            size = align_len[gene]
            start = last_position + 1  # Start position in NEXUS file
            end = size + start - 1  # End position in NEXUS file
            partitions_output += f"DNA, {gene} = {start}-{end}\n"
            paritions_nexus_output += f"    charset part{gene_no} = {self.prefix}_{gene}.fna.align: 1-{size};\n"
            last_position = end  # Update last position
        paritions_nexus_output += "END;\n"

        # Write NEXUS matrix to file
        with open(f"{self.prefix}_matrix.nexus", "w") as handle:
            handle.write(nexus_output)

        # Write partitioning information to separate files
        with open(f"{self.prefix}_partitions.txt", "w") as handle:
            handle.write(partitions_output)
        with open(f"{self.prefix}_partitions.nexus", "w") as handle:
            handle.write(paritions_nexus_output)

#######################
# AUXILIARY FUNCTIONS #
#######################

def remove_trailing_Ns(sequence):
	sequence = re.sub(r"[-]+", "", sequence).strip().upper()
	sequence = re.sub(r"^[N\?]+", "", sequence)
	sequence = re.sub(r"[N\?]+$", "", sequence)
	return sequence

def file_exists(file_path):
    """
    Check if a given file exists.
    Parameters:
        file_path (str): Path to the file.
    Returns:
        bool: True if the file exists, False otherwise.
    """
    return os.path.isfile(file_path)  # Check if the file exists in the specified path

def directory_exists(directory_path):
    """
    Check if a given directory exists.
    Parameters:
        directory_path (str): Path to the directory.
    Returns:
        bool: True if the directory exists, False otherwise.
    """
    return os.path.isdir(directory_path)  # Check if the directory exists in the specified path

def reverse_complement(dna_sequence):
    """
    Generate the reverse complement of a DNA sequence.
    Parameters:
        dna_sequence (str): Original DNA sequence.
    Returns:
        str: Reverse complement of the DNA sequence.
    """
    return str(Seq(dna_sequence).reverse_complement())  # Use BioPython to reverse-complement the sequence

def split_codons(dna_sequence):
    """
    Split a DNA sequence into codons (triplets).
    Parameters:
        dna_sequence (str): DNA sequence.
    Returns:
        list of str: List of codons, where each codon is a 3-base string.
    """
    # Iterate over sequence in steps of 3 and collect only complete codons
    return [dna_sequence[i:i+3] for i in range(0, len(dna_sequence), 3) if len(dna_sequence[i:i+3]) == 3]

def find_substring(text, substring):
    """
    Locate the start and end positions of a substring within a string.
    Parameters:
        text (str): The main text in which to search.
        substring (str): The substring to find.
    Returns:
        tuple: Start and end positions of the substring in the main text.
    """
    start = text.find(substring)  # Find the start position of the substring
    end = start + len(substring) - 1  # Calculate the end position
    return start, end

def delete_empty_files(output_prefix='output'):
    """
    Delete any files in the current directory that are empty.
    Input:
        None
    Output:
        None (Deletes empty files as a side effect).
    """
    # Iterate over each file in the current directory
    for filename in glob.glob(f"{output_prefix}*"):
        # Check if file size is zero and delete if true
        if os.path.getsize(filename) == 0:
            os.remove(filename)
    return

def parsl_clean(output_prefix='output', runinfo_dir='runinfo'):
    """
    Clean up Parsl's run information directory and delete any empty files in the current directory.
    Parameters:
        runinfo_dir (str): Directory where Parsl stores run information. Defaults to 'runinfo'.
    Returns:
        None (Deletes files and directories as a side effect).
    """
    # Remove Parsl run info directory if it exists
    if os.path.exists(runinfo_dir) and os.path.isdir(runinfo_dir):
        shutil.rmtree(runinfo_dir)

    # Delete any empty files in the current directory
    for filename in glob.glob(f"{output_prefix}*"):
        if os.path.getsize(filename) == 0:
            os.remove(filename)
    return

def check_output(output, overwrite):
    """
    Check for existing output files to avoid overwriting unless overwrite is specified.
    Parameters:
        output (str): Prefix for output files.
        overwrite (bool): If True, allows overwriting of existing files.
    Returns:
        None (Displays warning or error messages based on conditions).
    """
    # Find any existing output files with the specified prefix
    output_files = glob.glob(f"{output}*")

    # If files exist and overwrite is enabled, display a warning
    if len(output_files) > 0 and overwrite:
        sys.stderr.write(f"!WARNING: {len(output_files)} pre-existing output files may be overwritten.\n")
        return
    # If files exist and overwrite is disabled, display an error and stop execution
    elif len(output_files) > 0:
        sys.stderr.write(f"!STOP: Detected {len(output_files)} pre-existing output files; stopping to prevent overwriting (see the --overwrite option).\n")
        exit()

##############
# FIND LORFS #
##############

def find_lorf(input_dna):
    """
    Identify the longest open reading frame (ORF) in the given DNA sequence.
    Parameters:
        input_dna (str): DNA sequence in which to search for the longest ORF.
    Returns:
        tuple: Start position, end position, reading frame, and sequence of the longest ORF.
    """
    best_lorf = ""  # Stores the longest ORF found
    best_frame = 0  # Stores the reading frame of the longest ORF found

    # Test each of the three reading frames
    for frame in range(3):
        forward = input_dna[frame:]  # Adjust DNA sequence based on the frame
        remainder = len(forward) % 3  # Check if length is divisible by 3 for full codons

        # Truncate the sequence if there’s a remainder
        if remainder != 0:
            forward = forward[:-remainder]

        # Generate the reverse complement for the reverse strand search
        reverse = reverse_complement(forward)

        # Search for ORFs in both forward and reverse sequences
        fwd_lorf = codon_search(forward)
        rev_lorf = codon_search(reverse)

        # Update best_lorf if a longer ORF is found in the current frame
        if len(fwd_lorf) >= len(best_lorf) and len(fwd_lorf) >= len(rev_lorf):
            best_lorf = fwd_lorf
            best_frame = frame + 1  # Positive frame for forward direction
        elif len(rev_lorf) >= len(best_lorf):
            best_lorf = rev_lorf
            best_frame = -1 * (frame + 1)  # Negative frame for reverse direction

    # Determine ORF start and end positions based on the reading frame direction
    if best_frame > 0:  # Positive frame
        lorf_start, lorf_end = find_substring(input_dna, best_lorf)
    else:  # Negative frame, use reverse complement
        lorf_start, lorf_end = find_substring(reverse_complement(input_dna), best_lorf)

    # Check if the ORF length is divisible by 3 (for codon alignment)
    if (lorf_end - lorf_start + 1) % 3 != 0:
        sys.stdout.write(f"!ERROR: Polyprotein prediction length (from {lorf_start} to {lorf_end}) is not divisible by 3.\n")
        exit()
    else:
        # Return start, end positions, frame, and ORF sequence
        return lorf_start, lorf_end, best_frame, best_lorf

def codon_search(sequence):
    """
    Search for open reading frames (ORFs) by splitting a sequence into codons 
    and grouping non-stop codons together.
    Parameters:
        sequence (str): DNA sequence to search for ORFs.
    Returns:
        str: Longest ORF sequence found in the input DNA sequence.
    """
    codons = split_codons(sequence)  # Split sequence into codons (triplets)
    orf_dic = {}  # Dictionary to store ORFs by index
    orf_count = 0  # ORF index counter

    # Iterate over each codon to group them into ORFs
    for codon in codons:
        if codon in stop_codons:  # Stop codon marks the end of an ORF
            orf_count += 1  # Increment ORF index for the next ORF
        else:
            # If ORF exists in dictionary, add the codon to it
            if orf_count in orf_dic:
                orf_dic[orf_count].append(codon)
            else:
                orf_dic[orf_count] = [codon]  # Start a new ORF list

    # Find the longest ORF and return it as a joined string
    return "".join(max(orf_dic.values(), key=len))

############
# CHECKING #
############

def check_one_plus(value):
    """
    Verify that the given value is a positive integer greater than or equal to 1.
    Parameters:
        value (str): String representing the value to check, which should be convertible to an integer.
    Returns:
        int: The integer value if it is 1 or greater.
    Raises:
        SystemExit: If the value is less than 1, exits with an error message.
    """
    ivalue = int(value)  # Convert string value to an integer
    if ivalue < 1:
        # Print an error message and exit if the value is less than 1
        sys.stderr.write(f"!ERROR: Threads must be equal to or greater than 1; got {value}.\n")
        exit()
    else:
        return ivalue  # Return the integer if it passes the check

########
# MAIN #
########

def main():
    """
    Main function to execute the program: handles argument parsing, file checks, 
    and initiates either BLAST/HMMER runs or sequence alignments as specified by user options.
    Input:
        None
    Output:
        Runs either the BLAST/HMMER processes or the sequence alignment based on user arguments. 
        Creates various output files with results.
    """

    # Set up command-line argument parser
    parser = argparse.ArgumentParser(description="Fast Loci Annotation of Viruses (FLAVi): Version 2.")
    # Define arguments for the input file, directories, output, and other options
    parser.add_argument("-i", "--input", type=str, default="example.fasta", help="Path to the input file in FASTA format (default = example.fasta).", required=False)
    parser.add_argument("-p", "--peptides", type=str, default="peptides", help="Path to FLAVi's peptide directory (default = peptides)", required=False)
    parser.add_argument("-m", "--hmms", type=str, default="hmms", help="Path to FLAVi's HMMs directory (default = hmms)", required=False)
    parser.add_argument("-o", "--output", type=str, default="output", help="Prefix for all output file names (default = output)", required=False)
    parser.add_argument("-t", "--threads", type=check_one_plus, default=4, help="Maximum number of threads to use (default = 4)", required=False)
    parser.add_argument("--noblast", help="Do not execute BLAST searches", action="store_true")
    parser.add_argument("--nohmmer", help="Do not execute HMMER searches", action="store_true")
    parser.add_argument("--overwrite", help="Writes on top of existing output files", action="store_true")
    parser.add_argument("-a", "--align", help="Runs MAFFT to perform translation-based alignments of all .fna and all .faa files in the current directory that have the specified output prefix (requires more than one entry per input file).", action="store_true")

    # Parse command-line arguments
    args = parser.parse_args()

    # Clean previous Parsl run information if it exists
    parsl_clean(args.output)

    # Check for required input files and directories
    error = False  # Track if any errors are found
    if not file_exists(args.input):
        sys.stderr.write(f"!ERROR: Cannot find file '{args.input}'.\n")
        error = True
    if not directory_exists(args.peptides):
        sys.stderr.write(f"!ERROR: Cannot find directory '{args.peptides}'.\n")
        error = True
        # Verify specific peptide files exist in the peptides directory
        for peptide_file in ["2k.pep", "capsid.pep", "envelope.pep", "membrane.pep", "npro.pep", "ns1.pep", "ns2.pep", "ns3.pep", "ns4a.pep", "ns4b.pep", "ns5.pep", "p7.pep", "pr.pep"]:
            if not file_exists(f"{args.peptides}/{peptide_file}"):
                sys.stderr.write(f"!ERROR: Cannot find file '{peptide_file}' in '{args.peptides}'.\n")
                error = True
    if not directory_exists(args.hmms):
        sys.stderr.write(f"!ERROR: Cannot find directory '{args.hmms}'.\n")
        error = True
        # Verify HMM file exists in the HMMs directory
        if not file_exists(f"{args.hmms}/FLAVi.hmm"):
            sys.stderr.write(f"!ERROR: Cannot find file 'FLAVi.hmm' in '{args.hmms}'.\n")
            error = True

    # If any required files or directories are missing, exit the program
    if error:
        exit()

    # Create Parsl configuration for parallel processing
    parsl_config_dict = create_parsl_config(args.threads)
    parsl_config = parsl.config.Config(**parsl_config_dict)
    parsl.load(config=parsl_config)  # Load Parsl configuration

    # If alignment mode is not selected, perform BLAST and/or HMMER processing
    if not args.align:
        parsl_clean()  # Clean previous Parsl run information if it exists
        check_output(args.output, args.overwrite)  # Check if output files exist and handle based on overwrite flag

        # Initialize Flavi object for processing
        run = Flavi(args.input, args.peptides, args.hmms, args.output, args.threads)
        run.read()  # Read input sequences
        run.find_lorfs()  # Find longest ORFs in each sequence
        run.print_lorfs()  # Print ORFs to output files

        # Run BLAST if the "noblast" flag is not set
        if not args.noblast:
            run.blast()

        # Run HMMER if the "nohmmer" flag is not set
        if not args.nohmmer:
            run.hmmer()
        run.output_clean()  # Clean intermediate output files
        run.calculate_predictions()  # Calculate predictions and export results
        run.export_fasta()  # Export final sequences in FASTA format
        # Clean up Parsl run information and empty files
        parsl_clean()

    # If alignment mode is selected, perform alignment processes
    elif args.align:
        # Initialize Align object for handling alignments
        run = Align(args.output, args.threads)
        run.read_fna()  # Read nucleotide sequences from .fna files
        run.align_faa()  # Perform protein alignment with MAFFT
        run.read_faa_alignments()  # Read aligned protein sequences
        run.prot2nucl()  # Convert aligned proteins to nucleotide alignments
        run.fasta2nexus()  # Export nucleotide alignments in NEXUS format
        # Clean up Parsl run information and empty files
        parsl_clean()
    return

###################
# GUARD STATEMENT #
###################

if __name__ == "__main__":
    main()  # Run main function if this script is executed directly
