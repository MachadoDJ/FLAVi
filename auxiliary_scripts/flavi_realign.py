#!/usr/bin/env python3

import argparse, re, parsl, sys, os, shutil
from parsl import python_app, bash_app, ThreadPoolExecutor
from Bio import SeqIO

def create_parsl_config(threads):
	'''
	Create and return a Parsl configuration with ThreadPoolExecutor
	Input:
		threads (int)
	Output:
		Parsl configuration
	'''
	return {
		"executors": [
			ThreadPoolExecutor(
				max_threads=threads,
				label='local_threads'
			)
		],
		"strategy": None
	}

@bash_app
def align_with_mafft(filename, mafft_threads, stdout=None, stderr=None, shell=True):
	return f"mafft --auto --thread {mafft_threads} {filename} > {filename}.aln 2> {filename}.err"

def parse_nexus(nexus_file):
	'''
	This function receives the name of a NEXUS file that it will open and from which it will extract character partition information.
	Input:
		nexus_file (str)
	Output:
		sets (dic)
	'''
	try:
		handle = open(nexus_file, 'r')
		file_content = handle.read()
		handle.close()
		pattern = re.compile(r'charset\s+(\w+)\s*=\s*(\d+)-(\d+);', re.IGNORECASE)
		sets = {}
		for match in pattern.finditer(file_content):
			name = match.group(1)
			start = int(match.group(2))
			end = int(match.group(3))
			sets[name] = [start, end]
	except:
		print(f"Error: Could not read {nexus_file}.")
		exit()
	else:
		return sets

def get_files(files, option):
	'''
	This function opens the input file and extracts the each FASTA file name with its corresponding NEXUS partitions file.
	Input:
		files (str)
	Output:
		file_dic (dic), sequences_id_file (str)
	'''
	file_dic = {}
	sequences_id_file = None
	option_arguments = {'rmv', 'RMV'}
	try:
		with open(files, 'r') as handle:
			lines = [line.strip() for line in handle.readlines() if line.strip()]
			if option in option_arguments and len(lines) > 1:
				fasta, nexus = lines[0].split()
				file_dic[1] = [fasta, nexus]
				sequences_id_file = lines[1]
			else:
				count = 1
				for line in lines:
					parts = line.split()
					if len(parts) < 2:
						raise ValueError(f"Error: Could not read {files}. Not enough values to unpack (expected 2, got {len(parts)})")
					fasta, nexus = parts
					file_dic[count] = [fasta, nexus]
					count += 1
	except:
		print(f"Error: Could not read {files}.")
		exit()
	else:
		return file_dic, sequences_id_file

def parse_fasta(fasta_file, sets, prefix, sequence_ids_file, option):
	'''
	This function reads sequences in FASTA format and breaks them down into individual genes, which are also written in FASTA format.
	Input:
		fasta_file (str)
		sets (dic)
		prefix (str)
		sequence_ids_file (list)
		option (str)
	Output:
		gene_files (list)
	'''
	genes_dic = {}
	remove_mode = option in ['RMV', 'rmv']
	sequence_ids = set()
	if remove_mode:
		with open(sequence_ids_file, 'r') as id_file:
			sequence_ids = {line.strip() for line in id_file if line.strip()}
	with open(fasta_file, "r") as file:
		for record in SeqIO.parse(file, "fasta"):
			entry_name = str(record.description)
			if remove_mode and entry_name not in sequence_ids:
				continue
			sequence = str(record.seq).upper()
			for gene_name in sets:
				start, end = sets[gene_name]
				if start > len(sequence) or end > len(sequence):
					continue
				gene_sequence = sequence[start - 1:end]
				if gene_name not in genes_dic:
					genes_dic[gene_name] = {}
				if entry_name in genes_dic[gene_name]:
					genes_dic[gene_name][entry_name] += gene_sequence  
				else:
					genes_dic[gene_name][entry_name] = gene_sequence 
	gene_files = []
	for gene_name in genes_dic:
		output_fasta = ""
		for entry_name in genes_dic[gene_name]:
			output_fasta += f">{entry_name}\n{genes_dic[gene_name][entry_name]}\n"
		output_file_name = f"{prefix}_{gene_name}.fasta"
		if output_file_name not in gene_files:
			gene_files.append(output_file_name)
		with open(output_file_name, 'a') as handle:
			handle.write(output_fasta)
	return gene_files

def combine_alignments(gene_order, prefix):
	alignments = {}
	sizes = {}
	terminals = []
	for gene_name in gene_order:
		aln_file = f"{prefix}_{gene_name}.fasta.aln"
		for record in SeqIO.parse(aln_file, "fasta"):
			entry_name = str(record.description)
			sequence = str(record.seq).upper()
			if gene_name not in sizes:
				sizes[gene_name] = len(sequence)
			if gene_name not in alignments:
				alignments[gene_name] = {}
			alignments[gene_name][entry_name] = sequence
			if entry_name not in terminals:
				terminals.append(entry_name)
	fasta_output = ""
	for entry_name in terminals:
		sequence = ""
		for gene_name in gene_order:
			if entry_name in alignments[gene_name]:
				sequence += alignments[gene_name][entry_name]
			else:
				sequence += "N" * sizes[gene_name]
		fasta_output += f">{entry_name}\n{sequence}\n"
	handle = open(f"{prefix}_align.fasta", "w")
	handle.write(fasta_output)
	handle.close()
	nexus_output = "#NEXUS\n\nbegin sets;\n"
	x = 1
	for gene_name in sizes:
		start = x
		end = x + sizes[gene_name] - 1
		nexus_output += f"\tcharset {gene_name} = {start}-{end};\n"
		x = end + 1
	nexus_output += "end;\n"
	handle = open(f"{prefix}_partitions.nexus", "w")
	handle.write(nexus_output)
	handle.close()
	return

def cleanup_files(prefix, gene_order):
	'''
	This function receives a list of genes passed by the user and removes '.fasta', '.fasta.aln', and '.fasta.err' files of genes not listed.
	Input:
		prefix (str)
		gene_order (list)
	'''
	all_genes = ["C", "pr", "M", "E", "NS1", "NS2", "NS3", "NS4A", "NS4B", "NS5"]

	genes_not_listed = list(set(all_genes) - set(gene_order))
	genes_not_listed.sort(key=all_genes.index)

	for gene_name in genes_not_listed:
		files_to_remove = [
			f"{prefix}_{gene_name}.fasta",
			f"{prefix}_{gene_name}.fasta.aln",
			f"{prefix}_{gene_name}.fasta.err"
		]
		for file in files_to_remove:
			os.remove(file)


def main(files, prefix, threads, gene_order, mafft_threads, option):
	'''
	This is this script's main function, and it receives the name of a file with textual information.
	Input:
		files (str)
		prefix (str)
		threads (int)
		gene_order (list)
		mafft_threads (int)
		option (str)
	'''
	file_dic, sequence_ids = get_files(files, option)
	complete_jobs = []
	for n in sorted(file_dic):
		fasta_file = file_dic[n][0]
		nexus_file = file_dic[n][1]
		sets = parse_nexus(nexus_file)
		gene_files = parse_fasta(fasta_file, sets, prefix, sequence_ids, option)
		if not gene_files:
			continue
		for i in range(0, len(gene_files), threads):
			chunk = gene_files[i:i + threads]
			my_jobs = [align_with_mafft(filename, mafft_threads) for filename in chunk]
			complete_jobs.extend([result.result() for result in my_jobs])
	for gene_name in gene_order:
		fasta_file = f"{prefix}_{gene_name}.fasta"
		err_file = f"{prefix}_{gene_name}.fasta.err"
		if os.path.exists(fasta_file):
			os.remove(fasta_file)
		if os.path.exists(err_file):
			os.remove(err_file)
	combine_alignments(gene_order, prefix)
	if len(gene_order) > 0:
		cleanup_files(prefix, gene_order)
	return

if __name__ == '__main__':
	parser = argparse.ArgumentParser(description='Given several FASTA files and corresponding partitions in Nexus format, this script will help you re-align each partition and construct a new concatenated alignment.')
	parser.add_argument('-f', '--files', type=str, default='files.txt', help='A text file with two columns separated by tabs (left: FASTA file, right: corresponding NEXUS partition file). Default = files.txt', required=False)
	parser.add_argument('-o', '--option', type=str, default='add', choices=['add', 'ADD', 'rmv', 'RMV'], help='Options for realignment. "add or ADD" for realigning multiple files, and "rmv or RMV" for realigning specific set of sequences', required=False)
	parser.add_argument('-p', '--prefix', type=str, default='output', help='Prefix for output file names. Default = output', required=False)
	parser.add_argument('-t', '--threads', type=int, default=4, help="Maximum number of threads (default = 4)", required=False)
	parser.add_argument('-m', '--mafft_threads', type=int, default=-1, help="Value for MAFFT's threads argument (default = -1)", required=False)
	parser.add_argument('-g', '--genes', nargs="*", default=["C", "pr", "M", "E", "NS1", "NS2", "NS3", "NS4A", "NS4B", "NS5"], help="Order of genes, space separated (must match partitions fill) (default = C pr M E NS1 NS2 NS3 NS4A NS4B NS5)", required=False)
	args = parser.parse_args()
	files = args.files
	prefix = args.prefix
	threads = args.threads
	gene_order = args.genes
	mafft_threads = args.mafft_threads
	parsl_config_dict = create_parsl_config(threads)
	parsl_config = parsl.config.Config(**parsl_config_dict)
	parsl.load(config=parsl_config)
	main(args.files, args.prefix, args.threads, args.genes, args.mafft_threads, args.option)
	parsl_dir = "runinfo"
	if os.path.exists(parsl_dir) and os.path.isdir(parsl_dir):
		shutil.rmtree(parsl_dir)
	