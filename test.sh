#!/bin/bash

# Use this script to test your installation of FLAVi.
# You may want to adjust the maximum number of threads (MAX_THREADS, below) to your liking.

MAX_THREADS=12
OUTPUT_PREFIX="output"

function clean {
	# This function deletes previous output files, if they exist.
	echo "Preparing for a fresh start..."
	for filename in "${OUTPUT_PREFIX}"* ; do
		if [ -f ${filename} ] ; then
			echo "Removing file ${filename}..."
			rm ${filename}
		fi
	done
	echo "Done"
}

function predict {
	# First stage: annotations.
	echo "Running predictions... "
	time \
		python3 FLAVi2.py \
			-i example.fasta \
			-t ${MAX_THREADS} \
			--overwrite
	echo "Done"
}

function align {
	# Second stage: alignments.
	echo "Running alignments...."
	time \
	python3 FLAVi2.py \
		-i example.fasta \
		-t ${MAX_THREADS} \
		--align
	echo "Done"
}

echo "Start: `date`"

clean # Start fresh

predict # Stage 1: Predictions only

align # Stage 2: Requires completed stage 1, and performs alignments

echo "End: `date`"

exit
