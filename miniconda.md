# Documentation for Preparing and Using a Virtual Environment for FLAVi2 with Miniconda
[toc]

## Overview

Your can use Miniconda to simplify the environment setup for FLAVi2. Miniconda enables you to create isolated virtual environments —these are dedicated spaces for specific Python and package versions, preventing conflicts and ensuring reproducibility. Changes made in a virtual environment won’t affect your global Python installation or other environments.

This guide has been tested on Ubuntu versions 22.04 and 24.04 with amd64 architecture.

## Download and Install Miniconda

1. Download the latest Miniconda3 installer:
```
wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh
```

2. Run the installer:
```
bash Miniconda3-latest-Linux-x86_64.sh
```

3. Scroll through the license agreement, review it, and accept the terms if you agree.

4. Confirm installation settings. 

Choose the installation directory, typically:
```
/home/your_username/miniconda3
```

Decide whether to update your shell profile to automatically initialize conda when a new terminal session starts.

If you accept it and rather undo this option, run:
```
conda config --set auto_activate_base false
```

## Use Miniconda

When you start using conda, you have a default environment called `base`. To activate this environment, run:
```
conda activate
```
To exit the currently active conda environment, use:
```
conda deactivate
```
With those commands, your prompt should look like:

`(base): $`

It is of best practice not to install programs or packages into your `base` environment. Instead, create separate environments for different projects to maintain isolation and prevent conflicts. To create a new environment named `flavi2_env` with Python version 3.11.8, run:

```
conda create -n flavi2_env python=3.11.8
```

To activate the `flavi2_env` environment, use:
```
conda activate flavi2_env
```

Once the `flavi2_env` environment is active, install all FLAVi2 required dependencies with:
```
conda install -c conda-forge biopython=1.80 pandas=2.1.4 parsl=2024.05.13
conda install -c bioconda blast=2.15.0 hmmer=3.4 mafft=7.526
```

After the installation, your FLAVi2 environment will be ready for use.

## Remove Environments and Miniconda
To remove a conda virtual environment, first deactivate it.
```
conda deactivate
```

Then, remove the conda environment with:
```
conda remove --name your_conda_environment --all
```
Replace your_conda_environment with the name of the environment you wish to delete.

If you wish to uninstall Miniconda, delete the Miniconda3 installation directory. It will be typically located in your home directory:
```
rm -rf ~/miniconda3
```

Next, remove the Conda initialization commands from your shell configuration profile (e.g., `~/.bashrc`). **Warning**: Do **not** delete any other content or the configuration file itself.

The block is marked with "conda initialize" comments. It should look something like the following:
```
# >>> conda initialize >>>
# !! Contents within this block are managed by 'conda init' !!
__conda_setup="$('/home/your_username/miniconda3/bin/conda' 'shell.bash' 'hook' 2> /dev/null)"
if [ $? -eq 0 ]; then
    eval "$__conda_setup"
else
    if [ -f "/home/your_username/miniconda3/etc/profile.d/conda.sh" ]; then
        . "/home/your_username/miniconda3/etc/profile.d/conda.sh"
    else
        export PATH="/home/your_username/miniconda3/bin:$PATH"
    fi
fi
unset __conda_setup
# <<< conda initialize <<<
```
You can use a text editor like `nano` or `vim` to edit the file. For example:
```
nano ~/.bashrc
```
After removing the "conda initialize" block, save and close the file. This step completes the process of removing Conda from your shell.