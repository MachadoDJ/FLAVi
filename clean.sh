#!/bin/bash

# This script shelps you removing output files if you want to clean up.

OUTPUT_PREFIX="output"

function clean {
	for filename in "${OUTPUT_PREFIX}"* ; do
		if [ -f ${filename} ] ; then
			rm ${filename}
		fi
	done
}

clean # Start fresh

exit
