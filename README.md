# FLAVi: Fast Loci Annotation of Viruses

**Version**: 2.0  (October 15, 2024)
**Author**: [Dr. Denis Jacob Machado](http://phyloinformatics.com/)

[toc]

## Overview

FLAVi2 (Fast Loci Annotation of Viruses 2) is a Python3 application designed for fast, automated annotation and sequence alignment of viral genomes of the family *Flaviviridae*. It integrates tools like `BLAST`, `HMMER`, `MAFFT`, and `Parsl` for concurrent processing. This pipeline provides functionality for annotating loci, translating polyproteins, and aligning nucleotide and protein sequences for phylogenetic analysis.

---

## Cite

FLAVi is available from GitLab ([click here](https://gitlab.com/MachadoDJ/FLAVi)). If you use FLAVi in your research, please cite the following article:

- Jacob Machado, D., de Bernadi Schneider, A., Guirales, S., & Janies, D. A. (2020). FLAVi: An enhanced annotator for viral genomes of *Flaviviridae*. *Viruses*, **12(8)**, 892. DOI: [10.3390/v12080892](https://doi.org/10.3390/v12080892).

---

## License

FLAVi is made available to you via a GNU LESSER GENERAL PUBLIC LICENSE (Version 3, 29 June 2007). Please read the `LICENSE.txt` file for details.

---

## Software dependencies

FLAVi depends on the following Python libraries and external tools:
- [**Python**](https://www.python.org/): developed on version 3.11.8
- [**BioPython**](https://biopython.org/): developed with version 1.80
- [**Pandas**](https://pandas.pydata.org/): developed with version 2.1.4
- [**Parsl**](https://parsl-project.org/): developed with version 2024.05.13
- [**BLAST+**](https://blast.ncbi.nlm.nih.gov/Blast.cgi): developed with version 2.15.0+
- [**HMMER**](http://hmmer.org/): developed with version 3.4 (Aug 2023)
- [**MAFFT**](https://mafft.cbrc.jp/alignment/software/): developed with version is 7.526 (2024/Apr)

Ensure these packages are installed and accessible in your environment.

## Miniconda

To simplify environment setup, you can use Miniconda to manage FLAVi2 dependencies. This tutorial was tested on Ubuntu v22.04 and v24.04, with amd64 architecture.

**1.** Download the latest Miniconda3 installer:
```
wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh
```

**2.** Install it with:
```
bash Miniconda3-latest-Linux-x86_64.sh
```

**3.** Set up a new environment and specify the required dependencies:
```
conda create -n flavi2_env python=3.11.8
conda activate flavi2_env
conda install -c conda-forge biopython=1.80 pandas=2.1.4 parsl=2024.05.13
conda install -c bioconda blast=2.15.0 hmmer=3.4 mafft=7.526
```

After installation, you’ll see `(flavi2_env) $` at your prompt, confirming that the FLAVi2 environment is activated and ready to be used.

For a more detailed tutorial, read `miniconda.md`.

## Docker container

One alternative if you do not want to install all of these dependencies is to get FLAVi2 in a Docker container.

FLAVi2 comes with a `Dockerfile` document in case you want to build that container yourself. But you can also pull a multi-platform (OS: Ubuntu v24.04, architecture: linux/amd64 or linux/arm64) container from Docker Hub with the command `docker pull phyloinformatics2022/flavi2`.

If you are using Singularity, you can pull that container through it to create a `sif` image using the command `singularity pull flavi2.sif docker://phyloinformatics2022/flavi2:20241015`.

Read `DOCKER.md` for more details about the container and how to use it.



---

## You will also need these

- You will need a directory named `hmms` with the profile hmm named `FLAVi.hmm`. It comes with FLAVi2.
- You will also need a directory named `peptides` with a number of amino acids sequences in FASTA format. It also comes with FLAVi2.

The contained (see above) already has these materials inside.

---

## Usage

### Command-Line Options

The script can be run with the following arguments:

| Argument           | Description                                                  | Default         |
| ------------------ | ------------------------------------------------------------ | --------------- |
| `-i`, `--input`    | Path to the input file in FASTA format                       | `example.fasta` |
| `-p`, `--peptides` | Path to FLAVi's peptide directory                            | `peptides`      |
| `-m`, `--hmms`     | Path to FLAVi's HMMs directory                               | `hmms`          |
| `-o`, `--output`   | Prefix for all output file names                             | `output`        |
| `-t`, `--threads`  | Maximum number of threads to use                             | `4`             |
| `--noblast`        | Skip BLAST searches                                          | Disabled        |
| `--nohmmer`        | Skip HMMER searches                                          | Disabled        |
| `--overwrite`      | Overwrite existing output files                              | Disabled        |
| `-a`, `--align`    | Run MAFFT for translation-based alignments on `.fna` and `.faa` files | Disabled        |

### Example Usage

FLAVi2 can be executed in two steps. The first step (default) will identify the longest open reading frame in each input sequence and proceed to predict its genes using BLAST and HMMER. For example:

```bash
python3 FLAVi2.py -i example.fasta -t 4 --overwrite
```

After this step is concluded, the user may wich to produce translation-based alignments using MAFFT. For that, simply use the argument option `-a ` or `--align`. For example:

```
python3 FLAVi2.py -i example.fasta -t 4 --align
```

---

## Summary of the computational code

### 1. Setting Up
   - **1.1** Imports necessary Python libraries (`argparse`, `glob`, `os`, etc.) and attempts to import BioPython, Pandas, and Parsl libraries.
   - **1.2** Sets stop codons for identifying ORFs.
   - **1.3** Defines the Parsl configuration function to manage parallel/serial execution.

### 2. Class Definitions
   - **2.1** `Flavi` Class:
      - **2.1.1** **Purpose**: Manages tasks related to genome annotation.
      - **2.1.2** **Functions**:
         - **2.1.2.1** `read`: Reads genome sequences from the input file.
         - **2.1.2.2** `find_lorfs`: Identifies the longest ORFs in each genome sequence.
         - **2.1.2.3** `print_lorfs`: Saves identified ORFs to separate files.
         - **2.1.2.4** `blast`: Runs BLASTP search for each ORF against peptide sequences.
         - **2.1.2.5** `hmmer`: Searches for HMM profiles in each ORF using HMMER.
         - **2.1.2.6** `output_clean`: Deletes generated temporary files.
         - **2.1.2.7** `calculate_predictions`: Calculates predictions based on BLAST and HMMER results and saves them.
         - **2.1.2.8** `export_fasta`: Exports identified ORF nucleotide and protein sequences in FASTA format.
   
   - **2.2** `Align` Class:
      - **2.2.1** **Purpose**: Handles sequence alignment and file formatting.
      - **2.2.2** **Functions**:
         - **2.2.2.1** `read_fna`: Reads nucleotide sequences for alignment.
         - **2.2.2.2** `align_faa`: Aligns protein sequences using MAFFT.
         - **2.2.2.3** `read_faa_alignments`: Reads aligned protein sequences.
         - **2.2.2.4** `prot2nucl`: Converts aligned protein sequences back to nucleotide alignments.
         - **2.2.2.5** `fasta2nexus`: Exports aligned sequences to a NEXUS file.

### 3. Auxiliary Functions
   - **3.1** Define functions to check for file/directory existence, compute reverse complements, split DNA into codons, find substrings, delete empty files, clean up Parsl directories, and check for output conflicts.

### 4. Finding Longest ORFs (FIND LORFS)
   - **4.1** `find_lorf`: Identifies the longest ORF in a DNA sequence by testing each reading frame.
   - **4.2** `codon_search`: Breaks down a sequence into ORFs by grouping codons.

### 5. Main Workflow (MAIN)
   - **5.1** Cleans temporary files and directories.

   - **5.2** **Argument Parsing**:
      - Defines and parses command-line arguments for input files, output prefix, thread count, and options.
      
   - **5.3** **Initial Checks**:
      - Checks for required input files and directories.
      - Exits if any required resource is missing.
      
   - **5.4** **Set Up and Run Parsl**:
      - Sets up Parsl configuration based on the number of threads specified.
      - Loads the Parsl configuration.
      
   - **5.5** **Annotation or Alignment**:
      - If `--align` is not specified, performs genome annotation by:
         - Reading genomes, finding ORFs, printing ORFs, and executing BLASTP and HMMER.
         - Calculating predictions and exporting results in FASTA format.
         
      - If `--align` is specified, performs alignment tasks by:
         - Reading nucleotide files, aligning protein files, converting alignments, and exporting a NEXUS matrix.
         
---

## Simplified workflows

### Predictions and annotations
1. **Setup & Imports**: Imports required libraries, sets up configuration for Parsl, and handles any necessary library loading.
2. **Parse Arguments**: Parses the command-line arguments, using default options as specified.**
3. **Initial Checks**: Verifies the existence of the required input files and directories, exiting if any are missing.
4. **Set Up Parsl**: Configures Parsl for thread usage based on the default argument (4 threads) and loads the Parsl configuration.
5. **Execute Annotation (Flavi Class)**: Reads genome sequences, finds the longest ORFs, and saves ORFs to individual files.
6. **BLAST Search**: Runs BLASTP for each ORF, comparing against peptide files, and stores results.
7. **HMMER Search**: Runs HMMER for each ORF, identifying matching profiles, and stores results.
8. **Output Predictions**: Processes results to calculate final predictions and writes them to a CSV file.
9. **Export FASTA Files**: Exports annotated ORFs as nucleotide and protein FASTA files for each genome.

```
 ┌───────────────────────────────┐
 │           START               │
 └───────────────┬───────────────┘
                 │
    ┌────────────▼─────────────┐
    │ 1. Setup & Imports       │
    │    - Imports libraries   │
    │    - Loads Parsl config  │
    └────────────┬─────────────┘
                 │
    ┌────────────▼───────────────┐
    │2. Parse Arguments          │
    │    - Use default options   │
    └────────────┬───────────────┘
                 │
 ┌───────────────▼───────────────┐
 │3. Initial Checks              │
 │    - Confirm input files and  │
 │      directories exist        │
 └───────────────┬───────────────┘
                 │
 ┌───────────────▼───────────────┐
 │4. Set Up Parsl                │
 │    - Configure thread usage   │
 │    - Load Parsl config        │
 └───────────────┬───────────────┘
                 │
 ┌───────────────▼───────────────┐
 │5. Execute Annotation (Flavi)  │
 │   Class                       │
 │                               │
 │  - Read genome sequences      │
 │  - Find longest ORFs          │
 │  - Save ORFs to files         │
 └───────────────┬───────────────┘
                 │
 ┌───────────────▼───────────────┐
 │6. BLAST Search                │
 │   - For each ORF, run BLASTP  │
 │   - Store results in output   │
 └───────────────┬───────────────┘
                 │
 ┌───────────────▼───────────────┐
 │7. HMMER Search                │
 │   - For each ORF, run HMMER   │
 │   - Store results in output   │
 └───────────────┬───────────────┘
                 │
 ┌───────────────▼───────────────┐
 │8. Output Predictions          │
 │   - Calculate predictions     │
 │   - Write results to CSV      │
 └───────────────┬───────────────┘
                 │
 ┌───────────────▼───────────────┐
 │9. Export FASTA Files          │
 │   - Export annotated ORFs in  │
 │     FASTA format              │
 └───────────────┬───────────────┘
                 │
    ┌────────────▼─────────────┐
    │         END              │
    └──────────────────────────┘
```

### Alignment

1. **Setup & Imports**: Imports required libraries, sets up configuration for Parsl, and handles any necessary library loading.

2. **Parse Arguments**: Parses the command-line arguments, with `--align` specified to activate alignment functions.

3. **Initial Checks**: Verifies the existence of required input files and directories, exiting if any are missing.

4. **Set Up Parsl**: Configures Parsl for thread usage based on the argument (default 4 threads) and loads the Parsl configuration.

5. **Execute Alignment (Align Class)**:

   - Reads nucleotide sequences from `.fna` files.

   - Aligns protein sequences in `.faa` files using MAFFT.

   - Reads aligned protein sequences from `.faa.align` files.

   - Converts aligned protein sequences back to nucleotide alignments.

   - Exports the aligned nucleotide sequences to a NEXUS format file.

```
 ┌───────────────────────────────┐
 │           START               │
 └───────────────┬───────────────┘
                 │
    ┌────────────▼─────────────┐
    │ 1. Setup & Imports       │
    │    - Imports libraries   │
    │    - Loads Parsl config  │
    └────────────┬─────────────┘
                 │
    ┌────────────▼───────────────┐
    │2. Parse Arguments          │
    │    - Parse with --align    │
    │      flag enabled          │
    └────────────┬───────────────┘
                 │
 ┌───────────────▼───────────────┐
 │3. Initial Checks              │
 │    - Confirm input files and  │
 │      directories exist        │
 └───────────────┬───────────────┘
                 │
 ┌───────────────▼───────────────┐
 │4. Set Up Parsl                │
 │    - Configure thread usage   │
 │    - Load Parsl config        │
 └───────────────┬───────────────┘
                 │
 ┌───────────────▼───────────────┐
 │5. Execute Alignment (Align)   │
 │   Class                       │
 │                               │
 │   - Read nucleotide sequences │
 │   - Align protein sequences   │
 │   - Read aligned protein      │
 │     sequences                 │
 │   - Convert aligned proteins  │
 │     back to nucleotide        │
 │   - Export alignments to      │
 │     NEXUS format              │
 └───────────────┬───────────────┘
                 │
    ┌────────────▼─────────────┐
    │         END              │
    └──────────────────────────┘
```

---



## Description of output files

When running the code successfully, the following output files are generated, organized according to their types and functions. Each file is prefixed by a customizable label, which defaults to `output_`. You can adjust this prefix using the `-o` or `--output` option. Here’s what each file represents:

### File Types and Purposes

1. **Nucleotide and Protein FASTA Files**
   - **`.fna` files**: These files contain nucleotide sequences in FASTA format.
   - **`.faa` files**: These files contain protein sequences in FASTA format.
   - For both types, an additional `.align` version may be present, indicating the aligned sequence files that include gaps (`-`) to represent the sequence alignments.
2. **Concatenated Alignment and Partitions**
   - **`output_matrix.nexus`**: This file provides a concatenated alignment of all nucleotide sequences in NEXUS format, commonly used for phylogenetic analyses. Parts of the genomes may be missing in this alignment. This is not the same as a whole genome alignment. This alignment represents the aligned partition, concatenated in the same file.
   - **`output_partitions.nexus`**: A NEXUS-format file listing the individual alignments of each nucleotide sequence for easy reference in phylogenetic tools. This partitions file follows the style commonly used in the program IQ-Tree.
   - **`output_partitions.txt`**: A partitions file formatted for use with RAxML. This file corresponds to `output_matrix.nexus`, indicating where each alignment begins and ends within the concatenated matrix.
3. **Predictions File**
   - **`output_predictions.csv`**: Contains final annotations and predictions derived from BLAST and HMMER searches. This CSV includes details such as start and end positions, e-values, and scores for each prediction. The table will show not only the best predictions from BLAST and HMMER but also will give you the most conserved predictions in nucleotide (`Nucl_Start` and `Nucl_End`) and amino acid (`Prot_Start` and `Prot_End`) space based on that. The goals is to maximize the chance that the prediction includes the correct sequence.
