# Documentation for Preparing and Running the Docker Container for FLAVi2
[toc]

## The Dockerfile

The following are the contents of FLAVI2's original Dockerfile.

```
# Base image
FROM ubuntu:24.04

# Set metadata labels
LABEL maintainer="Dr. Denis Jacob Machado <dmachado@charlotte.edu>"
LABEL description="This Docker image installs Python 3.11 with dependencies for bioinformatics, including BioPython, Parsl, BLAST+, HMMER, and MAFFT. It also includes a custom script FLAVi2.py with the necessary input files and directories."

# Set environment variables
ENV DEBIAN_FRONTEND=noninteractive
ENV PATH="/usr/local/bin:$PATH"

# Update and install essential tools, Python, and bioinformatics software
RUN apt-get -y update
RUN apt-get install -y software-properties-common
RUN add-apt-repository ppa:deadsnakes/ppa -y
RUN apt-get -y update

# Install BLAST and HMMER
RUN apt install ncbi-blast+ -y
RUN apt install hmmer -y
RUN hmmbuild -h

# Install python3 and pip3
RUN apt-get install -y python3
RUN apt-get install -y python3-pip

# Install the latest versions of Python dependencies with pip
RUN pip3 install --break-system-packages biopython pandas parsl

# Set the working directory to root
WORKDIR /

# Copy files and directories to root
COPY FLAVi2.py .
COPY example.fasta .
COPY peptides ./peptides
COPY hmms ./hmms

# Preprocess HMMER database with hmmpress
RUN cd hmms && hmmpress -f FLAVi.hmm > /dev/null 2>&1

# Clean up
RUN apt-get clean && rm -rf /var/lib/apt/lists/*

# Set the default command to run the main Python script with the specified arguments
CMD ["python3", "FLAVi2.py", "-i", "example.fasta", "-p", "peptides", "-m", "hmms"]
```

## Creating and Setting Up a Multi-Platform Build Environment

## How to built is as  a multi-platform container

Docker Desktop can’t work with multi-platform images by default. We need to enable the “**containerd”** image store. The “**containerd”** image store isn't enabled by default.

[containerd image store](https://docs.docker.com/desktop/containerd/#what-is-containerd)

> **Containerd** is **an open-source, mid-level container runtime that manages the entire container lifecycle on a single host**. It's a daemon that's available for Linux and Windows and is designed to be easily integrated into larger systems.

To enable the feature for Docker Desktop, do the following:

1. Navigate to settings in Docker Desktop.
2. In the general tab, check the “**containerd”** option (it may read something like “Use containerd for pulling and storing images”).
3. Apply and restart.

After that, you should check the availability of the specific driver you will need:

```bash
$ docker info -f '{{ .DriverStatus }}'
```

The output should read something like `driver-type io.containerd.snapshotter.v1`. If it does, you are ready to continue building your image in multi-platform mode. Here is an example of how to do just that:

To build the `FLAVi2` container image with support for multiple platforms, we use Docker Buildx, which extends Docker’s capabilities to support multi-architecture builds.

- **Command**: `docker buildx create --use`

- **Explanation**: This command creates a new Buildx builder instance and sets it as the default (`--use`). Buildx enables cross-platform builds (e.g., `amd64`, `arm64`) and supports Docker Desktop as well as other environments. This command configures the environment to use Buildx for subsequent Docker build commands.

## Initializing and Bootstrapping the Builder Instance

- **Command**: `docker buildx inspect --bootstrap`
- **Explanation**: This command inspects the current Buildx builder instance and bootstraps it (`--bootstrap`) to prepare for multi-platform builds. Bootstrapping initializes the builder environment to ensure it’s ready to perform cross-platform builds.

## Checking Docker System Information

- **Command**: `docker info -f '{{ .DriverStatus }}'`
- **Explanation**: This command retrieves detailed Docker system information, formatted to show only the `DriverStatus` field. This is useful for verifying the Docker environment’s storage driver and other statuses, which helps confirm that the Buildx setup is functioning correctly.

## Building a Multi-Platform Docker Image

- **Command**: `docker buildx build --platform linux/amd64,linux/arm64 -t flavi2 --output type=docker .`

- **Explanation:** This command builds a multi-platform Docker image for both `amd64` and `arm64` architectures.
  - **`--platform linux/amd64,linux/arm64`**: Specifies the target architectures.
  - **`-t flavi2`**: Tags the resulting image with the name `flavi2`.
  - **`--output type=docker`**: Configures Buildx to save the image directly to the local Docker daemon rather than pushing it to a registry, allowing for immediate local use.

## Listing Docker Images

- **Command**: `docker images`

- **Explanation**: Displays a list of all images stored locally on your Docker system, including the `flavi2` image built in the previous step. This command allows you to verify that the image exists and check details like repository, tag, image ID, and size.

## Removing a Docker Image

- **Command**: `docker rmi flavi2`
- **Explanation**: Deletes the `flavi2` image from your local Docker environment. This is useful for cleaning up old or unneeded images to save disk space or prepare for rebuilding the image. If the image is in use by a container, Docker will prevent deletion unless you add the `-f` (force) option.

## Running the FLAVi2 Python Script in the Container with Local Input and Output Directories

To run FLAVi2.py in the container, with a local input file and an output directory mounted to access results directly on the host:
```bash
docker run -it --rm -v "$(pwd)/genomes.fasta:/genomes.fasta" -v "$(pwd)/output:/output" flavi2 python3 FLAVi2.py -i genomes.fasta -p peptides -m hmms -o /output/output --overwrite
```

**Explanation**:

- **`-it`**: Runs the container in interactive mode with a terminal, allowing for input if necessary.
- **`--rm`**: Automatically removes the container after it exits, keeping the environment clean.
- **`-v "$(pwd)/genomes.fasta:/genomes.fasta"`**: Mounts the local `genomes.fasta` file to `/genomes.fasta` in the container, allowing the container to access the local file as input.
- **`-v "$(pwd)/output:/output"`**: Mounts the local `output` directory to `/output` in the container, enabling the container’s output to be saved directly to the host’s `output` directory.
- **`flavi2`**: Specifies the name of the image to run.
- `python3 FLAVi2.py -i genomes.fasta -p peptides -m hmms -o /output/output --overwrite`: Runs the `FLAVi2.py` script with the following arguments:
  - **`-i genomes.fasta`**: Points to the input file.
  - **`-p peptides`** and **`-m hmms`**: Specifies the peptides and hmms directories in the container.
  - **`-o /output/output`**: Designates the output file path in the mounted `/output` directory.
  - **`--overwrite`**: Allows the script to overwrite any existing output files, if this flag is supported by `FLAVi2.py`.

## Availability

The FLAVi2 Docker image is available in Docker Hub. You can use the following Docker pull command to get it:

```bash
docker pull phyloinformatics2022/flavi2
```



## Pulling the Docker contained using Singularity

If you are using Singularity, you can pull the Docker image from the online repository to create a `sif` image.

```bash
singularity pull flavi2.sif docker://phyloinformatics2022/flavi2:20241015
```

