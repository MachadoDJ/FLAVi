>ns4a id:NC_031327
ASVNWEGLIQGWWHVATKSDFSLIKQAFFNTMERLHDLSRWDDDAMRSSDMTESVGTWLV
VAVTAVTTFAFSIMIFWCYRCCRSSKATRSQEIMYASAVSERGVSGAWSSMTVPVLGWVA
GIPGPILFIAAICLGLVCAFMCNSSTR
>ns4a id:NC_012932
SSASLYDILFSFDWHGIWKKTTSSLWDLRDIVSGDLHDQILAEQSLTSGMAFMLGCAIAV
ALLMFLWVFTCLVSYSRSGKNSFEPMPVSDPLGGGFFLTSPGVLHYFGVPLGFCVIIFLA
MFIVYPVLYKAAGNR
>ns4a id:NC_015843
SAIGILEVFRMLPDHFAHRMTESMDNIYMLTTAEKGSRAHREALEELPETLETFLLVFMM
TVASMGVFLFFVQRRGLGKTGLGAMVMATVTVLLWIAEVPAQKIAGVLLVSLLLMIVLIP
EPERQR
>ns4a id:NC_001475
SIALDLVTEIGRVPSHLAHRTRNALDNLVMLHTSEDGGRAYRHAVEELPETMETLLLLGL
MILLTGGAMLFLISGKGIGKTSIGLICVIASSGMLWMAEVPLQWIASAIVLEFFMMVLLI
PEPEKQR
>ns4a id:NC_001474
SLTLNLITEMGRLPTFMTQKARDALDNLAVLHTAEAGGRAYNHALSELPETLETLLLLTL
LATVTGGIFLFLMSGRGIGKMTLGMCCIITASILLWYAQIQPHWIAASIILEFFLIVLLI
PEPEKQR
>ns4a id:NC_001477
SVSGDLILEIGKLPQHLTQRAQNALDNLVMLHNSEQGGKAYRHAMEELPDTIETLMLLAL
IAVLTGGVTLFFLSGRGLGKTSIGLLCVIASSALLWMASVEPHWIAASIILEFFLMVLLI
PEPDRQR
>ns4a id:NC_012532
GAALGVMEALGTLPGHMTERFQEAIDNLAVLMRAETGSRPYKAAAAQLPETLETIMLLGL
LGTVSLGIFFVLMRNKGIGKMGFGMVTLGASAWLMWLSEIEPARIACVLIVVFLLLVVLI
PEPEKQR
>ns4a id:NC_002640
SITLDILTEIASLPTYLSSRAKLALDNIVMLHTTERGGRAYQHALNELPESLETLMLVAL
LGAMTAGIFLFFMQGKGIGKLSMGLITIAVASGLLWVAEIQPQWIAASIILEFFLMVLLI
PEPEKQR
>ns4a id:NC_009026
SVTTGLLEGVGRLPEHLGQRLKESIDTLYLAFTAEVGSRPHREAMQEMPAALETVLVFFL
LMIMTGCTFFLLMRHKGINKMGYGMVVMSAVGGLLWYGNVPAPKIAGILLLTFLLMVVLI
PNPEKQR
>ns4a id:NC_001563
SQIGLVEVLGRMPEHFMVKTWEALDTMYVVATAEKGGRAHRMALEELPDALQTIVLIALL
SVMSLGVFFLLMQRKGIGKIGLGGVILGAATFFCWMAEVPGTKIAGMLLLSLLLMIVLIP
EPEKQR
>ns4a id:NC_040788
DLLTVIGGLPGYMNEKWMNAMDTLYILWSGDSSSRAYREAMNSLPEALEVAITVALATIV
TLGIFFVLMRSKGMSKMTVGLITMMFASALLLWAEVPAAQVAGLVVVMFILMVVLVPDSE
KQR
>ns4a id:NC_027819
SVTVVDFVMAVQHFIVSGAFKQKFFEVLESAYIATRLGDDDIPTMKKDKMISALMAIWMG
GIFAVILYLVILLFLSLGKYITRDKNPQVINQYDEIPKAWGTCWALLSYHMGVPFAMIFV
IGGAITLIKIIAGNNTTR
>ns4a id:NC_027817
SFITFATLSHVISQVIEAGVVNSAWKRIGDVSIIFTEGGDPHAKDETIMAWTILVGGVLG
ALGFLIVAWGMKAVLRVIFGSRDKHLSVPTLVADFQPYIVCIVPIALHLAGVPIPMTIVF
FAMLFLTYPLMYKSAGQR
>ns4a id:NC_017086
SYVPIIEVLGKLPQHFADKTIDAADTFKTVLTATPGSRAYRLAVDNLPDAAETAIFVTMV
GFMTMGILIFLMAPKGMTRMSLGFMTIMAATYFLWASGMAGYQIAAMQLVAFILFVVLVP
EPGSQR
>ns4a id:NC_016997
SYMPILDVVGKLPQHFSDRAIDAADTIRTVLTANPDSRSYRLAIDNLPEAAETAMLIGML
VSVTMGSIMFLMMPKGITRMSLGLIVMIMATWFMWTSGMAGYQIAAVQLLAFVFFLVLVP
EPGNQR
>ns4a id:NC_012671
SVLNFDVLGVAHGLYVALTSINMEMLGNTFRDTIENLHVISNVDDPYVSDFTMGQSLNAW
TAVLIGAAASTILLLTVVGMYKIVCWLFGRRPDQPSSAPPTIITYAQGGISQLGSMAIAI
APMCAVLVGIPPVFVFIAVVGLFVIMSVNATNVHR
>ns4a id:NC_005064
SSQTFYDLFKSIDWANIWKKTLSALWDIRDTFSGTLKDQVMAERSLTTGMAFSLGILLVL
AVIVVVWILGFLVAMLKPTKMSYESMPSGDPFNGGMVLTAPSVLHYMGVPLGFCVIIFLA
MFIVYPVLYKAIGNR
>ns4a id:NC_027709
SFGDVLTGMSGVPELLRHRCVSALDVFYTLMHEEPGSRAMKMAERDAPEAFLTVVEMMVL
GLATLGVVWCFVVRTSISRMVLGTLVLLASLLLLWAGGVGYGSMAGVALIFYTLLTVLQP
ETGKQR
>ns4a id:NC_005062
SLGDMLTGMSGVPELLRHRCMSAMDVFYTLLYEEPGSRAMKMAERDAPEAFLTMVEMVVL
GLATLGAVWCLVLRTSISRMMLGTMVLLVSLALLWAGGVGYGSMAGVALVFYTLLTVLQP
EAGKQR
>ns4a id:NC_003690
SVGDVLMGMSGVPALLRQRCTSAMDVFYTLMHEEPGSRAMRMAERDAPEAFLTAVEMLVL
GLATLGVVWCFVVRTSVSRMVLGTLVLATSLIFLWAGGVGYGNMAGVALVFYTLLTVLQP
ETGKQR
>ns4a id:NC_003635
SSVWLSLPGALYNQFTEAMDTIYVYYTANPSSKGFRMAQESMPTALLTVMQALIIGTGLI
MFLGWMCSSRKVDRMMLGTLLIVGCSVTAWCGGVPLPLVSAMALVTFILLLCLVPEEGQQ
R
>ns4a id:NC_001672
SFGDVLTGMSGVPELLRHRCVSALDVFYTLMHEEPGSRAMRMAERDAPEAFLTMVEMMVL
GLATLGVIWCFVVRTSISRMMLGTLVLLASLLLLWAGGVGYGNMAGVALIFYTLLTVLQP
EAGKQR
>ns4a id:NC_002031
GAAEVLVVLSELPDFLAKKGGEAMDTISVFLHSEEGSRAYRNALSMMPEAMTIVMLFILA
GLLTSGMVIFFMSPKGISRMSMAMGTMAGCGYLMFLGGVKPTHISYVMLIFFVLMVVVIP
EPGQQR
>ns4a id:NC_006551
SAVGFLEVLGRMPEHFAGKTREAFDTMYLVATAEKGGKAHRMALEELPDALETITLIVAL
AVMTAGVFLLLVQRRGIGKLGLGGMVLGLATFFLWMADVSGTKIAGTLLLALLMMIVLIP
EPEKQR
>ns4a id:NC_040788 start:6417 end:6846
DLLTVIGGLPGYMNEKWMNAMDTLYILWSGDSSSRAYREAMNSLPEALEVAITVALATIV
TLGIFFVLMRSKGMSKMTVGLITMMFASALLLWAEVPAAQVAGLVVVMFILMVVLVPDSE
KQR
>ns4a id:KY320648 start:6417 end:6846
DLLTVIGGLPGYMNEKWMNAMDTLYILWSGDSSSRAYREAMNSLPEALEVAITVALATIV
TLGIFFVLMRSKGMSKMTVGLITMMFASALLLWAEVPAAQVAGLVVVMFILMVVLVPDSE
KQR
>ns4a id:KY272991 start:6464 end:6845
AAFGVMEALGTLPGHMTERFQEAIDNLAVLMRAETGSRPYKAAAAQLPETLETIMLLGLL
GTVSLGIFFVLMRNKGIGKMGFGMVTLGASAWLMWLSEIEPARIACVLIVVFLLLVVLIP
EPEKQR
>ns4a id:KU321639 start:6471 end:6912
FGVMEALGTLPGHMTERFQEAIDNLAVLMRAETGSRPYKAAAAQLPETLETIMLLGLLGT
VSLGIFFVLMRNKGIGKMGFGMVTLGASAWLMWLSEIEPARIACVLIVVFLLLVVLIPEP
EKQR
>ns4a id:NC_000943 start:6461 end:6839
SAIGFFEVLGRMPEHFAGKTREALDTMYLVATSEKGGKAHRMALEELPDALETITLIAAL
GVMTAGFFLLMMQRKGIGKLGLGALVLVVATFFLWMSDVSGTKIAGVLLLALLMMVVLIP
EPEKQR
>ns4a id:NC_006551 start:6462 end:6840
SAVGFLEVLGRMPEHFAGKTREAFDTMYLVATAEKGGKAHRMALEELPDALETITLIVAL
AVMTAGVFLLLVQRRGIGKLGLGGMVLGLATFFLWMADVSGTKIAGTLLLALLMMIVLIP
EPEKQR
>ns4a id:NC_001437 start:6464 end:6842
SAISFIEVLGRMPEHFMGKTREALDTMYLVATAEKGGKAHRMALEELPDALETITLIVAI
TVMTGGFFLLMMQRKGIGKMGLGALVLTLATFFLWAAEVPGTKIAGTLLIALLLMVVLIP
EPEKQR
>ns4a id:OP947882 start:6440 end:6818
SAGSVMEVMGRMPDYFWTKTLNAADNLYVLATANKGGRAHQAALEELPDTVETILLMTMM
CVASLGMFTLMVHRRGLGKTGLGTLVLATVTVLLWISDVPAPKIAGVLLIAFLLMIVLIP
EPEKQR
>ns4a id:OP947883 start:6441 end:6819
SAGSVMEVMGRMPDYFWTKTLNAADNLYVLATANKGGRAHQAALEELPDTVETILLMTMM
CVASLGMFALMVHRRGLGKTGLGTLVLATVTVLLWISDVPAPKIAGVLLIAFLLMIVLIP
EPEKQR
>ns4a id:OP947884 start:6441 end:6819
SAGSVMEVMGRMPDYFWTKTLNAADNLYVLATANKGGRAHQAALEELPDTVETILLMTMM
CVASLGMFALMVHRRGLGKTGLGTLVLATVTVLLWISDVPAPKIAGVLLIAFLLMIVLIP
EPEKQR
>ns4a id:OP947885 start:6441 end:6819
SAGSVMEVMGRMPDYFWTKTLNAADNLYVLATANKGGRAHQAALEELPDTVETILLMTMM
CVASLGMFALMVHRRGLGKTGLGTLVLATVTVLLWISDVPAPKIAGVLLIAFLLMIVLIP
EPEKQR
>ns4a id:OP947886 start:6440 end:6818
SAGSVMEVMGRMPDYFWTKTLNAADNLYVLATANKGGRAHQAALEELPDTVETILLMTMM
CVASLGMFALMVHRRGLGKTGLGTLVLATVTVLLWISDVPAPKIAGVLLIAFLLMIVLIP
EPEKQR
>ns4a id:OP947887 start:6441 end:6819
SAGSVMEVMGRMPDYFWTKTLNAADNLYVLATANKGGRAHQAALEELPDTVETILLMTMM
CVASLGMFALMVHRRGLGKTGLGTLVLATVTVLLWISDVPAPKIAGVLLIAFLLMIVLIP
EPEKQR
>ns4a id:OP947888 start:6441 end:6819
SAGSVMEVMGRMPDYFWTKTLNAADNLYVLATANKGGRAHQAALEELPDTVETILLMTMM
CVASLGMFALMVHRRGLGKTGLGTLVLATVTVLLWISDVPAPKIAGVLLIAFLLMIVLIP
EPEKQR
>ns4a id:OP947889 start:6441 end:6819
SAGSVMEVMGRMPDYFWTKTLNAADNLYVLATANKGGRAHQAALEELPDTVETILLMTMM
CVASLGMFALMVHRRGLGKTGLGTLVLATVTVLLWISDVPAPKIAGVLLIAFLLMIVLIP
EPEKQR
>ns4a id:OP947890 start:6441 end:6819
SAGSVMEVMGRMPDYFWTKTLNAADNLYVLATANKGGRAHQAALEELPDTVETILLMTMM
CVASLGMFALMVHRRGLGKTGLGTLVLATVTVLLWISDVPAPKIAGVLLIAFLLMIVLIP
EPEKQR
>ns4a id:OP947891 start:6441 end:6819
SAGSVMEVMGRMPDYFWTKTLNAADNLYVLATANKGGRAHQAALEELPDTVETILLMTMM
CVASLGMFALMVHRRGLGKTGLGTLVLATVTVLLWISDVPAPKIAGVLLIAFLLMIVLIP
EPEKQR
>ns4a id:KY347801 start:6405 end:6828
LDVFGKLPQHFSDRAIDAADTIRTVLTANPESRSYRLAIDNLPEAAETAMLIGMLVSVTM
GTIMFFMMPKGITRMSLGLIVMIMATWFMWTSGMAGYQIAAVQLLAFVFFLVLVPEPGNQ
R
>ns4a id:NC_002031 start:6439 end:6817
GAAEVLVVLSELPDFLAKKGGEAMDTISVFLHSEEGSRAYRNALSMMPEAMTIVMLFILA
GLLTSGMVIFFMSPKGISRMSMAMGTMAGCGYLMFLGGVKPTHISYVMLIFFVLMVVVIP
EPGQQR
>ns4a id:X03700 start:6439 end:7300
GAAEVLVVLSELPDFLAKKGGEAMDTISVFLHSEEGSRAYRNALSMMPEAMTIVMLFILA
GLLTSGMVIFFMSPKGISRMSMAMGTMAGCGYLMFLGGVKPTHISYVMLIFFVLMVVVIP
EPGQQR
>ns4a id:AJ242984 start:6343 end:6775
SSVWLSLPGALYNQFTEAMDTIYVYYTANPSSKGFRMAQESMPTALLTVMQALIIGTGLI
MFLGWMCSSRKVDRMMLGTLLIVGCSVTAWCGGVPLPLVSAMALVTFILLLCLVPEEGQQ
R
>ns4a id:NC_003635 start:6343 end:6706
SSVWLSLPGALYNQFTEAMDTIYVYYTANPSSKGFRMAQESMPTALLTVMQALIIGTGLI
MFLGWMCSSRKVDRMMLGTLLIVGCSVTAWCGGVPLPLVSAMALVTFILLLCLVPEEGQQ
R
>ns4a id:NC_004119 start:6345 end:6708
SSVLTAIPGLLYSRFLSAFDTIYIYSTSDPSSRAFKMAERELPEAILCVLQGFLMCVGML
ALIVWLVTRTKVDRMCIGFCVIVMSGIMAWIGGAPLSLVAALVLISFILLVCLIPEQGMQ
R
>ns4a id:NC_003675 start:6240 end:6603
SGVLLGLPRLMYQKVVESIDMVHTYYTADPNSRNFKLAEKELPDAFLCILQSLLMIVGVF
IILMWILSRTKVDRIWIGTLVIGMSGLTAWYGGVPLPVISGGALVCFVLLICLVPEEGMQ
R
>ns4a id:NC_003676 start:6240 end:6597
SGMTILQLLYEGLQHDLDVVYTYMRANKESRAWKMANEDLPEALVGLGQSVLSIVGVIFL
VWLLLRQSKVDRVTLGALLLTAGSAVLWMGGAAPCVVGGSLICGFVLLVALSPEEGMQR
>ns4a id:NC_005062 start:6462 end:6840
SLGDMLTGMSGVPELLRHRCMSAMDVFYTLLYEEPGSRAMKMAERDAPEAFLTMVEMVVL
GLATLGAVWCLVLRTSISRMMLGTMVLLVSLALLWAGGVGYGSMAGVALVFYTLLTVLQP
EAGKQR
>ns4a id:NC_001672 start:6462 end:6840
SFGDVLTGMSGVPELLRHRCVSALDVFYTLMHEEPGSRAMRMAERDAPEAFLTMVEMMVL
GLATLGVIWCFVVRTSISRMMLGTLVLLASLLLLWAGGVGYGNMAGVALIFYTLLTVLQP
EAGKQR
>ns4a id:NC_027709 start:6462 end:6909
SFGDVLTGMSGVPELLRHRCVSALDVFYTLMHEEPGSRAMKMAERDAPEAFLTVVEMMVL
GLATLGVVWCFVVRTSISRMVLGTLVLLASLLLLWAGGVGYGSMAGVALIFYTLLTVLQP
ETGKQR
>ns4a id:KP144332 start:6462 end:6909
SFGDVLTGMSGVPELLRHRCVSALDVFYTLMHEEPGSRAMKMAERDAPEAFLTVVEMMVL
GLATLGVVWCFVVRTSISRMVLGTLVLLASLLLLWAGGVGYGSMAGVALIFYTLLTVLQP
ETGKQR
>ns4a id:KP144331 start:6462 end:6909
SFGDVLTGMSGVPELLRHRCVSALDVFYTLMHEEPGSRAMKMAERDAPEAFLTVVEMMVL
GLATLGVVWCFVVRTSISRMMLGTLVLLASLLLLWAGGVSYGNMAGVALIFYTLLTVLQP
ETGKQR
>ns4a id:NC_001809 start:6459 end:6837
SFGDVLTGMSGVPELLRHRCVNALDVFYTLMHEEPGSRAMKMAERDAPEAFLTVVEMMVL
GLATLGVVWCFVVRTSISRMMLGTLVLLASLLLLWAGGVGYGNMAGVALIFYTLLTVLQP
ETGKQR
>ns4a id:EU790644 start:6460 end:6907
SVGDVLMGMSGVPALLRQRCTSAMDVFYTLMHEEPGSRAMRMAERDAPEAFLTAVEMLVL
GLATLGVVWCFVVRTSVSRMALGTLVLATSLIFLWAGGVGYGNMAGVALVFYTLLTVLQP
ETGKQR
>ns4a id:NC_003690 start:6460 end:6838
SVGDVLMGMSGVPALLRQRCTSAMDVFYTLMHEEPGSRAMRMAERDAPEAFLTAVEMLVL
GLATLGVVWCFVVRTSVSRMVLGTLVLATSLIFLWAGGVGYGNMAGVALVFYTLLTVLQP
ETGKQR
>ns4a id:NC_003687 start:6444 end:6822
SAVDILTGLGGVPDLLRLRCTAAWDVVYTLLNETPGSRAMKMAERDAPEAMLTLLEVAVL
GIATLGVVWCFIVRTSVSRMVLGTLVLAVALILLWLGGMDYGTMAGVALIFYLLLTVLQP
EPGKQR
>ns4a id:NC_001837 start:5292 end:5490
MAGPVLLVGLAMAGGALLAHWTGSIVVVTTWEVNGGGNPLLYQTRRGVPTSGSPVVVVPP
CEGGER
>ns4a id:NC_001710 start:5111 end:5309
DAGPILMIGLAIAGGMIYASYTGSLVVVTDWDVKGGGAPLYRHGDQATPQPVVQVPPVDH
RPGGES
>ns4a id:MH179063 start:4979 end:5177
DAGPILMVGLAIAGGMIYASYTGSLVVVTDWDVKGGGAPLYRHGDQATPQPVVQVPPVDH
RPGGES
>ns4a id:KC618401 start:5094 end:5292
DAGPILMVGLAIAGGMIYASYTGSLVVVTDWDVKGGGNPLYRSGDQATPQPVVQVPPVDH
RPGGES
>ns4a id:KC618398 start:5093 end:5291
DAGPILMVGLAIAGGMIYASYTGSLVVVTDWDVKGGGNPLYRSGDQATPQPVVQVPPVDH
RPGGES
>ns4a id:KC618400 start:5093 end:5291
DAGPILMVGLAIAGGMIYASYTGSLVVVSDWDVKGGGNPLYRSGDQATPQPVVQVPPVDH
RPGGES
>ns4a id:KC618399 start:5089 end:5287
DAGPILMVGLAIAGGMIYASYTGSLVVVTDWDVKGGGNPLYRSGDQATPQPVVQVPPVDH
RPGAES
>ns4a id:AJ238799 start:5312 end:5474
STWVLVGGVLAALAAYCLTTGSVVIVGRIILSGKPAIIPDREVLYREFDEMEEC
>ns4a id:D50481 start:5300 end:5462
STWVLVGGVLAALAAYCLTTGSVVIVGRIILSGKPAIIPDREVLYREFDEMEEC
>ns4a id:D50485 start:5300 end:5462
STWVLVGGVLAALAAYCLTTGSVVIVGRIILSGKPAIIPDREVLYREFDEMEEC
>ns4a id:AJ238800 start:4971 end:5134
STWVLVGGVLAALAAYCLTTGSVVIVGRIVLSGKPAIIPDREVLYREFDEMEEC
>ns4a id:D50484 start:5300 end:5462
STWVLVGGVLAALAAYCLTTGSVVIVGRIILSGKPAVIPDREVLYREFDEMEEC
>ns4a id:D50482 start:5300 end:5462
STWVLVGGVLAALAAYCLTTGSVVIVGRIILSGKPAVIPDREVLYREFDEMEEC
>ns4a id:MK527509 start:5304 end:5466
STWVLVGGVLAALAAYCLTTGSVVIVGRIILSGKPAIIPDREALYREFDEMEEC
>ns4a id:AJ000009 start:5295 end:5457
STWVLVGGVLAALAAYCLTTGSVVIVGRIILSGRPAIIPDREVLYQEFDEMEEC
>ns4a id:KT983617 start:5184 end:5346
STWVLVGGVLAALAAYCLTTGSVVIVGRIILSGKPAVIPDREVLYQEFDEMEEC
>ns4a id:FN435993 start:5313 end:5475
STWVLVGGVLAALAAYCLTTGSVVIVGRIILSGKPAIIPDREVLYQAFDEMEEC
>ns4a id:D50480 start:5300 end:5462
STWVLVGGVLAALTAYCLTTGSVVIVGRIILSGKPAVIPDREALYQEFDEMEEC
>ns4a id:D50483 start:5300 end:5462
STWVLVGGVLAALTAYCLTTGSVVIVGRIILSGKPAVIPDREALYQEFDEMEEC
>ns4a id:OQ832071 start:5295 end:5457
STWVLMGGVLAALAAYCLTVGCVVICGRIVVSGKPAVVPDREVLYQQFDEMEEC
>ns4a id:AY859526 start:5281 end:5443
STWVLVGGVLAALAAYCLSVGCVVICGRITLTGKPAVVPDREILYQQFDEMEEC
>ns4a id:DQ480522 start:5284 end:5446
STWVLVGGVLAALAAYCLSVGCVVICGRITLTGKPAVVPDREILYQQFDEMEEC
>ns4a id:DQ480521 start:5284 end:5446
STWVLVGGVLAALAAYCLSVGCVVICGRITLTGKPAVVPDREILYQQFDEMEEC
>ns4a id:DQ480520 start:5284 end:5446
STWVLVGGVLAALAAYCLSVGCVVICGRITLTGKPAVVPDREILYQQFDEMEEC
>ns4a id:DQ480518 start:5284 end:5446
STWVLVGGVLAALAAYCLSVGCVVICGRITLTGKPAVVPDREILYQQFDEMEEC
>ns4a id:DQ480517 start:5284 end:5446
STWVLVGGVLAALAAYCLSVGCVVICGRITLTGKPAVVPDREILYQQFDEMEEC
>ns4a id:DQ480516 start:5284 end:5446
STWVLVGGVLAALAAYCLSVGCVVICGRITLTGKPAVVPDREILYQQFDEMEEC
>ns4a id:DQ480513 start:5284 end:5446
STWVLVGGVLAALAAYCLSVGCVVICGRITLTGKPAVVPDREILYQQFDEMEEC
>ns4a id:DQ480512 start:5284 end:5446
STWVLVGGVLAALAAYCLSVGCVVICGRITLTGKPAVVPDREILYQQFDEMEEC
>ns4a id:DQ480524 start:5287 end:5449
STWVLVGGVLAALAAYCLSVGCVVICGRITLTGKPAVVPDREVLYQQFDEMEEC
>ns4a id:DQ480519 start:5284 end:5446
STWVLVGGVLAALAAYCLSVGCVVICGRITLTGKPAIVPDREILYQQFDEMEEC
>ns4a id:DQ480515 start:5284 end:5446
STWVLVGGVLAALAAYCLSVGCVVICGRITLTGKPAVVPDRETLYQQFDEMEEC
>ns4a id:DQ480514 start:5284 end:5446
STWVLVGGVLAALAAYCLSVGCVVICGRITLTGKPAVVPDRETLYQQFDEMEEC
>ns4a id:DQ480523 start:5284 end:5446
STWVIVGGVLAALAAYCLSVGCVVICGRITLTGKPVVVPDREVLYQQFDEMEEC
>ns4a id:HM049503 start:5311 end:5473
STWVLAGGVLAAVAAYCLATGCVSIIGRLHVNQRVVVAPDKEVLYEAFDEMEEC
>ns4a id:KY762287 start:7150 end:7342
STAENALLVALFSYVGYQALSKRHIPMVTDIYTIEDQRLEDTTHLQYAPNAIRTDGKETK
LEEL
>ns4a id:OP210314 start:7150 end:7342
STAENALLVALFSYVGYQALSKRHIPMVTDIYTIEDQRLEDTTHLQYAPNAIRTDGKETK
LEEL
>ns4a id:KY767958 start:7180 end:7372
STAENALLVALFSYVGYQALSKRHIPMVTDIYTIEDERLEDTTHLQYAPNAIRTDGKETK
LEEL
>ns4a id:OQ411020 start:7167 end:7359
STAENALLVALFSYVGYQALSKRHIPIVTDIYTIEDQRLEDTTHLQYAPNAIRTDGKETR
LEEL
>ns4a id:KY964311 start:7197 end:7389
SSAENALLIALFGYVGYQALSKRHVPMITDIYTIEDQRLEDTTHLQYAPNAIRTEGKETE
LKEL
>ns4a id:KT355592 start:7202 end:7394
SSAENALLIALFGYVGYQALSKRHVPMITDIYTIEDQRLEDTTHLQYAPNAIRTEGKETE
LKEL
>ns4a id:OR004805 start:7199 end:7391
SAENALLIALFGYVGYQALSKRHVPMITDIYTIEDQRLEDTTHLQYAPNAIRTEGKETEL
KEL
>ns4a id:OR004807 start:7198 end:7390
SAENALLIALFGYVGYQALSKRHVPMITDIYTIEDQRLEDTTHLQYAPNAIRTEGKETEL
KEL
>ns4a id:OR004808 start:7160 end:7352
SAENALLIALFGYVGYQALSKRHVPMITDIYTIEDQRLEDTTHLQYAPNAIRTEGKETEL
KEL
>ns4a id:OR004813 start:7199 end:7391
SAENALLIALFGYVGYQALSKRHVPMITDIYTIEDQRLEDTTHLQYAPNAIRTEGKETEL
KEL
>ns4a id:LC089875 start:7202 end:7394
SSAENALLIALFGYVGYQALSKRHVPMVTDIYTIEDQRLEDTTHLQYAPNAIRTEGKETE
LKEL
>ns4a id:KR866116 start:7212 end:7404
SSAENALLIALFGYVGYQALSKRHVPMVTDIYTIEDQRLEDTTHLQYAPNAIRTEGKETE
LKEL
>ns4a id:LC089876 start:7203 end:7395
SSAENALLIALFGYVGYQALSKRHVPMVTDIYTIEDQRLEDTTHLQYAPNAIRTEGKEAE
LKEL
>ns4a id:PP706246 start:7373 end:7565
SSAENALLIALFGYVGYQALSKRHIPMITDIYTIEDQRLEDTTHLQFAPNAIRTEGKETE
LKEL
>ns4a id:PP680566 start:7200 end:7392
SSAENALLIALFGYVGYQALSKRHVPMITDIYTIEDQRLEDTTHLQYAPNAIKTEGTETE
LKEL
>ns4a id:OM817562 start:7128 end:7320
STAENALLVALFGYVGYQALSKRHIPVVTDIYSIEDHRLEDTTHLQYAPNAIKTEGKETE
LKEL
>ns4a id:OM817566 start:7128 end:7320
STAENALLVALFGYVGYQALSKRHIPVVTDIYSIEDHRLEDTTHLQYAPNAIKTEGKETE
LKEL
>ns4a id:MG879027 start:7196 end:7388
STAENALLIALFGYVGYQTLSKRHIPMITDIYTLEDHRLEDTTHLQFAPNAIRTDGKDSE
LKEL
>ns4a id:LC006970 start:7565 end:7757
STAENALLIALFGYVGYQTLSKRHIPMITDIYTLEDHRLEDTTHLQFAPNAIRTDGKDSE
LKEL
>ns4a id:NC_038428
SWTGGLLGASAGLLLLYAATDTFGYFACTSSFNVVCRPELRLNETIDEPAIELEEC
>ns4a id:NC_038427
STTLTLAGVGLASAAVFAAVDLLGNIIIKHVWEKTTDSTAARVVEFEPLDTEEVLEEC
>ns4a id:NC_038432
WGAGVAASAASVFALGLALDYFGSVAITSAMILDSGEPPNAPEPADDDDGFEEC
>ns4a id:NC_038430
STGWVVAGASIAAICLLTEASASICITGIIRINDGKIFISQDRDNLYTILDEMEEC
>ns4a id:NC_031916
GGLALGVGFGVAMSVIAFDLMGSFSIRHAWKVTRGSTTLEDADMPAYLTDMGELEEC
>ns4a id:NC_040815
STTITLAGVGLAVAATYVAIDSFGNIVIKRCFEVTTDSTASLASQPIDDPLTSGLEEC
>ns4a id:NC_021153
SGYVTLAGVGLSMAAVAVAIDLLGNCCLKRTWEFTTDTTAARVVQPPEDDITENLEEC
>ns4a id:NC_004102
STWVLVGGVLAALAAYCLSTGCVVIVGRIVLSGKPAIIPDREVLYQEFDEMEEC
>ns4a id:NC_030791
STWVIVGGVLAAVAAYCMSTGSVVVVGRVVLGSNVVTAPDREVLYQHFDEMEEC
>ns4a id:NC_001655
SGTAALAVGVGVAMAYLAIDTFGATCVRRCWSITSVPTGATVAPVVDEEEIVEEC
>ns4a id:NC_009827
STWVIVGGVLAALAAYCLTVGCVVICGRIVTSGKPAVVPDREVLYQQFDEMEEC
>ns4a id:NC_009823
STWVLAGGVLAAVAAYCLATGCVCIIGRLHINQRAVVAPDKEVLYEAFDEMEEC
>ns4a id:NC_020902
TAAGPILLVGLAAAAALAIGDYTGSLVVVAEVVVEPGGNPVPPSGPIRYDGVPTGRAQDG
RVGPRPLDDDQVMEALEDV
>ns4a id:NC_021154
TTIGPLLLAGAAVAAAAAIADSTGCLVVTAVWEVGSGGSPLWPGSKTADERGDVQGGEAP
PGAQPPKVTPGEVKAAKDTAEVISEV
>ns4a id:NC_001837
LAGPVLLVGLAMAGGALLAHWTGSIVVVTTWEVNGGGNPLLYQTRRGVPTSGSPVVVVPP
CEGGER
>ns4a id:NC_038433
TSSGPILLAGLALAAAAAIADYTGTLVVVGTFDVRPGGAPRPPQSRDLPGGLSSGQPQSD
GEGPPPPRRTDQLTDSQTLDALQDV
>ns4a id:NC_001710
DAGPILMIGLAIAGGMIYASYTGSLVVVTDWDVKGGGAPLYRHGDQATPQPVVQVPPVDH
RPGGES
>ns4a id:NC_003679
SAAENALLVALFGYVGYQALSKRHVPMVTDIYSIEDHRLEDTTHLQFAPNAIRTDGKETE
LKEL
>ns4a id:NC_001461
SSAENALLVALFGYVGYQALSKRHVPMITDIYTIEDQRLEDTTHLQYAPNAIKTDGTETE
LKEL
>ns4a id:NC_002657
STAENALLVALFGYVGYQALSKRHIPVVTDIYSIEDHRLEDTTHLQYAPNAIKTEGKETE
LKEL
