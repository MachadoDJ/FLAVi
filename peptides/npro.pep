>npro id:KC533793 start:373 end:877
MELNHFELLYKTNKQKPMGVEEPVYDVAGRPLFGGSNEVHPQSTLKLPHDRGRGNIRTTL
KDLPRKGDCRSGNHLGPVSGIYVKPGPVFYQDYMGPVYHRAPLEFFDEAQFCEVTKRIGR
VTGSDGKLYHMYVCTDGCILLKLAKRGTPRTLKWIRNFTNCPLWVTSC
>npro id:KC533776 start:373 end:877
MELNHFELLYKTNKQKPMGVEEPVYDVTGRPLFGGSSEVHPQSTLKLPHDRGRGNIRTTL
KDLPRKGDCRSGNHLGPVSGIYVKPGPVFYQDYMGPVYHRAPLEFFDEAQFCEVTKRIGR
VTGSDGKLYHMYVCTDGCILLKLAKRGTPRTLKWIRNFTNCPLWVTSC
>npro id:KY762287 start:340 end:844
MELLNFELLYKTYKQKPAGVQEPLYDKNGAVLFGEPSDIHPQSTLKLPHSRGKKEVIVGI
RDLPRKGDCRTGNRLGPVSGLFVKPGPVFYQDYSGPVYHRAPLEQFKQAPMCETTKRIGR
VTGSDGNLYHMYVCTDGCILVKTAKREGQDVLKWVYNVLDSPIWVASC
>npro id:OP210314 start:340 end:844
MELLNFELLYKTYKQKPAGVQEPLYDKNGAVLYGEPSGIHPQSTLKLPHPRGEKEVIVGI
RDLPRKGDCRTGNRLGPVSGLFVKPGPVFYQDYSGPVYHRAPLEQFKQAPMCETTKRIGR
VTGSDGNLYHMYVCTDGCILVKTAKREGQDVLKWVYNVLDSPIWVASC
>npro id:KY767958 start:370 end:874
MELLNFELLYKTYKQKPAGVQEPLYDKNGAVLFGEPSDIHPQSTLKLPHPRGEKEVMVGM
RDLPRKGDCRSGNRLGPVSGLFVKPGPVFYQDYSGPVYHRAPLEQFKQARMCEVTKRIGR
VTGSDGNLYHMYVCTDGCILVKTAKREGQDILKWVYNVLDSPIWVASY
>npro id:OQ411020 start:357 end:861
MELLNFELLYKTYKQKPVGIQEPLYDKTGAVLFRERSDLHPQSTLKLPHPRGEKEVRVGI
RDLPRKGDCRTGNRLGPVSGLFVKPGPIYYQDYSGPVYHRAPLEQFEQAQMCEVTKRIGR
VTGSDGNLYHMYVCMDGCILVKTAKREGQETLKWVHNTLDSPIWVTSC
>npro id:MG879027 start:386 end:890
MELISNELLYKTYKQKPAGVVEPVYDIEGCPLFGESSEVHPQSTLKLPHQRGSANILTNA
RSLPQKGDCRKGNVNGAVSGIYIKPGPIYYQDYAGPVYHRAPLEYCREASMCEVTRRVGR
VTGSNGKLYHIYVCMDGCILLKRATRNQPEVLKWVYNRLNCPLWVTSC
>npro id:LC006970 start:386 end:887
MELFSNELLYKTYKQKPAGIVEPVYDINGCPLFGESSGIHPQSTLKLPHQRGSANILTNA
RSLPRKGDCRKGNVNGAVSGIYIKPGPVYYQDYAGPVYHRAPLELCREASMCETTRRIGR
VTGSNGNLYHIYVCIDGCILLKRAIRDQPEVLKWVYNRLNCPLWVTS
>npro id:KY964311 start:381 end:882
MELITNELLYKTYKQKPVGVEEPVYNQAGDPLFGERGVIHPQATLKLPHKRGEREVPTNL
ASLPKRGDCRSGNSKGPVSGIYLKPGPLFYQDYKGPVYHRAPLEFFQEASMCETTKRIGR
VTGSDGKLYHIYVCIDGCVIVKSATKYHQKVLKWVNNKLNCPLWVSS
>npro id:KT355592 start:386 end:887
MELITNELLYKTYKQKPTGVEEPVYDQAGNPLFGEKGEIHPQSTLKLPHKRGEREVPTNL
ASLPKRGDCRSGNSKGPVSGIYLKPGPLFYQDYKGPVYHRAPLEFFEEASMCETTRRIGR
VTGSDSKLYHIYVCIDGCIIIKSATKDRQKVLKWVHNKLNCPLWISS
>npro id:OR004805 start:380 end:881
MELITNELLYKTYKQKPTGVEEPVYDQAGNPLFGERGVIHPQSTLRLPHKRGEREVPTNL
ANLPKRGDCRSGNSKGPVSGIYLKPGPVFYQDYKGPVYHRAPLEFFEEASMCEITKRIGR
ITGSDSKLYHIYVCIDGCIIVKSATRDRQKVLKWVHNKLNCPLWVSS
>npro id:OR004807 start:379 end:880
MELITNELLYKTYKQKPTGVEEPVYDQAGNPLFGERGVIHPQSTLRLPHKRGEREVPTNL
ANLPKRGDCRSGNSKGPVSGIYLKPGPVFYQDYKGPVYHRAPLEFFEEASMCEITKRIGR
ITGSDSKLYHIYVCIDGCIIVKSATRDRQKVLKWVHNKLNCPLWVSS
>npro id:OR004808 start:341 end:842
MELITNELLYKTYKQKPTGVEEPVYDQAGNPLFGERGVIHPQSTLRLPHKRGEREVPTNL
ANLPKRGDCRSGNSKGPVSGIYLKPGPVFYQDYKGPVYHRAPLEFFEEASMCEITKRIGR
ITGSDSKLYHIYVCIDGCIIVKSATRDRQKVLKWVHNKLNCPLWVSS
>npro id:OR004813 start:380 end:881
MELITNELLYKTYKQKPTGVEEPVYDQAGNPLFGERGVIHPQSTLRLPHKRGEREVPTNL
ANLPKRGDCRSGNSKGPVSGIYLKPGPAFYQDYKGPVYHRAPLEFFEEASMCEITKRIGR
ITGSDSKLYHIYVCIDGCIIVKSATRDRQKVLKWVHNKLNCPLWVSS
>npro id:PP680566 start:384 end:888
MELISNELLYKTYKQKPVGVEEPVYDRGGNPLFGEKKEIHHQSTLKLPHKRGEREVPTNL
ASLPKRGDCRSGNSRGPVSGIYLKPGPQFYQDYVGPVYHRAPLELFEEGSMCETTRRIGR
VTGSDGKLYHIYVCIDGCIIVKSATRSHQKVLRWVHNKLNCPLWVTSC
>npro id:LC089875 start:386 end:890
MELISNELLYKTYKQKPLGVEEPVYDKVGNPLFGEKGEVHPLSTLKLPHRRGERDIPTNL
ASLPKRGDCRSGNSKGAVSGIYIKPGPVYYQDYKGPVYHRAPLEFFEEGLMCETTKRIGR
VTGSDGKLYHIYVCIDGCILVKSTTRDHQKVLKWVHNKLDCPLWVTSC
>npro id:PP706246 start:395 end:899
MEKISNELLYKTYKQKPLGVEEPVYDRAGNPLFGERGAVHPLSTLKLPHKRGERDVPTNL
VSLPRRGDCRSGNNKGPVSGIYVKPGPLYYQDYKGPVYHRAPLELFEEGPMCETTKRIGR
VTGSDGKLYHIYVCIDGCILVKSVSRDYQKVLKWIHNRLDCPLWVTSC
>npro id:KR866116 start:390 end:894
MELISNELLYKTYKQKPLGVEEPVYDKEGNPLFGERGTVHPQSTLKLPHKRGERDVSTNL
AHLPRRGDCRSGNSKGPVSGIYLKPGPMYYQDYKGPVYHRAPLELFEEGSMCETTKRIGR
VTGSDGKLYHIYVCIDGCILVKSASKDHQRVLKWIHNRLNCPLWVTSC
>npro id:LC089876 start:387 end:891
MELISNELLYKTYKQKPTGVEEPVYDRLGNPLFGERGEIHPQSTLKLPHKRGELEIPTNV
KSLPKRGDCRTGNNKGPVSGIYLKPGPQFYQDYTGPVYHRAPLELFEEGSMCEVTKRVGR
VTGSDGRLYHIYVCVDGCILVKSATRNNPKVLKWVHNRLNCPLWVTSC
